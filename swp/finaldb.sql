USE [master]
GO
/****** Object:  Database [SWP]    Script Date: 28/07/2024 12:30:42 SA ******/
CREATE DATABASE [SWP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SWP', FILENAME = N'D:\hoc\DBI202\MSSQL16.SQLEXPRESS\MSSQL\DATA\SWP.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SWP_log', FILENAME = N'D:\hoc\DBI202\MSSQL16.SQLEXPRESS\MSSQL\DATA\SWP_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [SWP] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SWP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SWP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SWP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SWP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SWP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SWP] SET ARITHABORT OFF 
GO
ALTER DATABASE [SWP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SWP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SWP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SWP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SWP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SWP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SWP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SWP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SWP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SWP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SWP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SWP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SWP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SWP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SWP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SWP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SWP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SWP] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SWP] SET  MULTI_USER 
GO
ALTER DATABASE [SWP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SWP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SWP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SWP] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SWP] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SWP] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [SWP] SET QUERY_STORE = ON
GO
ALTER DATABASE [SWP] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [SWP]
GO
/****** Object:  Table [dbo].[account]    Script Date: 28/07/2024 12:30:42 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[accountId] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[is_deleted] [bit] NULL,
	[roleId] [int] NULL,
	[phone] [varchar](20) NULL,
	[email] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[brand]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[brand](
	[brandId] [int] IDENTITY(1,1) NOT NULL,
	[brandName] [varchar](255) NOT NULL,
	[image] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[brandId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[category]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [nvarchar](255) NULL,
	[image] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chatBox]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chatBox](
	[chatId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[shopId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[chatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[color]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[color](
	[colorId] [int] IDENTITY(1,1) NOT NULL,
	[colorValue] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[colorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[commentId] [int] IDENTITY(1,1) NOT NULL,
	[accountId] [int] NULL,
	[shopProductId] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[content] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[commentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[discount]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[discount](
	[discountId] [int] IDENTITY(1,1) NOT NULL,
	[shopProductId] [int] NULL,
	[discountValue] [int] NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[promotionalPrice] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[discountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[images]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[images](
	[imagesId] [int] IDENTITY(1,1) NOT NULL,
	[shopProductId] [int] NULL,
	[imageLink] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[imagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message](
	[messageId] [int] IDENTITY(1,1) NOT NULL,
	[chatId] [int] NULL,
	[sentTime] [datetime] NULL,
	[senderId] [int] NULL,
	[content] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[messageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[order]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order](
	[orderId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[totalMoney] [money] NULL,
	[name] [varchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[phone] [varchar](20) NULL,
	[orderDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[orderDetail]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orderDetail](
	[orderDetaiId] [int] IDENTITY(1,1) NOT NULL,
	[orderId] [int] NULL,
	[quantity] [int] NULL,
	[price] [money] NULL,
	[productItemId] [int] NULL,
	[statusId] [int] NULL,
	[description] [nvarchar](255) NULL,
	[voucherId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderDetaiId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productItem]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productItem](
	[productItemId] [int] IDENTITY(1,1) NOT NULL,
	[shopProductId] [int] NULL,
	[sizeId] [int] NULL,
	[colorId] [int] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[productItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productLine]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productLine](
	[productLineId] [int] IDENTITY(1,1) NOT NULL,
	[productLineName] [nvarchar](255) NULL,
	[categoryId] [int] NULL,
	[brandId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[productLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rating]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rating](
	[ratingId] [int] IDENTITY(1,1) NOT NULL,
	[shopProductId] [int] NULL,
	[userId] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[starRating] [int] NULL,
	[orderDetailId] [int] NULL,
	[commentId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ratingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[roleName] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[shop]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shop](
	[shopId] [int] IDENTITY(1,1) NOT NULL,
	[shopName] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[image] [varchar](255) NULL,
	[accountBalance] [money] NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[shopId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[shopProduct]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shopProduct](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[shopId] [int] NULL,
	[productLineId] [int] NULL,
	[price] [money] NULL,
	[description] [nvarchar](255) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[is_deleted] [bit] NULL,
	[image] [varchar](255) NULL,
	[title] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[size]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[size](
	[sizeId] [int] IDENTITY(1,1) NOT NULL,
	[sizeValue] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[status]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[status](
	[statusId] [int] IDENTITY(1,1) NOT NULL,
	[statusValue] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[statusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[fullName] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[image] [varchar](255) NULL,
	[accountBalance] [money] NULL,
	[dob] [date] NULL,
	[gender] [bit] NULL,
	[accountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[voucher]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[voucher](
	[voucherId] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](20) NULL,
	[reducedAmount] [money] NULL,
	[endDate] [datetime] NULL,
	[isGlobal] [bit] NULL,
	[description] [nvarchar](255) NULL,
	[shopId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[voucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[voucherUser]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[voucherUser](
	[voucherUserId] [int] IDENTITY(1,1) NOT NULL,
	[voucherId] [int] NULL,
	[userId] [int] NULL,
	[quantity] [int] NULL,
	[isUsed] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[voucherUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wishlist]    Script Date: 28/07/2024 12:30:43 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wishlist](
	[wishListId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[shopProductId] [int] NULL,
	[createdAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[wishListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[account] ON 

INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (1, N'admin', N'mNy+OfwBVzPj9442CM86ANCQGd0=', CAST(N'2024-05-19' AS Date), NULL, 0, 1, N'0999999888', N'admin@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (2, N'user', N'VONcHg1wFhOU0vAVHAYsViNhnTU=', CAST(N'2024-05-19' AS Date), NULL, 0, 2, N'0943561200', N'hoangtran654@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (3, N'shop1', N'MTjz12pP83hutfr24fMHRBK0RXY=', CAST(N'2024-05-19' AS Date), NULL, 0, 3, N'0943561200', N'shop1@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (4, N'shop2', N'MUhkc4ri4TP7vOvFZ7cg+Diqvb8=', CAST(N'2024-05-23' AS Date), NULL, 0, 3, N'0984848484', N'shop2@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (5, N'hoangtran', N'qf8nByMUdU4+zhlp3kPB+bwO1fQ=', CAST(N'2024-06-06' AS Date), NULL, 0, 2, N'0948481235', N'hoang0948481235@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (6, N'shop3', N'6hBPVrJiazncmV8MZ09AQ9yhogE=', CAST(N'2024-06-08' AS Date), NULL, 0, 3, N'0999999999', N'shop3@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (7, N'shop4', N'6hBPVrJiazncmV8MZ09AQ9yhogE=', CAST(N'2024-06-08' AS Date), NULL, 0, 3, N'0888888888', N'shop4@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (8, N'shop5', N'6hBPVrJiazncmV8MZ09AQ9yhogE=', CAST(N'2024-06-08' AS Date), NULL, 0, 3, N'0989898999', N'shop5@gmail.com')
INSERT [dbo].[account] ([accountId], [username], [password], [created_at], [updated_at], [is_deleted], [roleId], [phone], [email]) VALUES (9, N'hoangthvhe', N'VONcHg1wFhOU0vAVHAYsViNhnTU=', CAST(N'2024-07-27' AS Date), NULL, 0, 2, N'0988888888', N'hoangthvhe173225@fpt.edu.vn')
SET IDENTITY_INSERT [dbo].[account] OFF
GO
SET IDENTITY_INSERT [dbo].[brand] ON 

INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (1, N'nike', N'images/nike-logo.jpg')
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (2, N'adidas', N'images/logo-adidas.png')
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (3, N'puma', N'images/logo-puma.png')
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (4, N'vans', N'images/logo-vans.png')
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (5, N'lemas', N'images/logo-lemans.png')
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (6, N'Timberland', NULL)
INSERT [dbo].[brand] ([brandId], [brandName], [image]) VALUES (7, N'THE WOLF', NULL)
SET IDENTITY_INSERT [dbo].[brand] OFF
GO
SET IDENTITY_INSERT [dbo].[category] ON 

INSERT [dbo].[category] ([categoryId], [categoryName], [image]) VALUES (1, N'Sneakers', N'')
INSERT [dbo].[category] ([categoryId], [categoryName], [image]) VALUES (2, N'Boots', N'')
INSERT [dbo].[category] ([categoryId], [categoryName], [image]) VALUES (3, N'Loafers', N'')
INSERT [dbo].[category] ([categoryId], [categoryName], [image]) VALUES (4, N'Sandals', N'')
INSERT [dbo].[category] ([categoryId], [categoryName], [image]) VALUES (5, N'Flip-flops', N'')
SET IDENTITY_INSERT [dbo].[category] OFF
GO
SET IDENTITY_INSERT [dbo].[chatBox] ON 

INSERT [dbo].[chatBox] ([chatId], [userId], [shopId]) VALUES (1, 1, 1)
SET IDENTITY_INSERT [dbo].[chatBox] OFF
GO
SET IDENTITY_INSERT [dbo].[color] ON 

INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (1, N'red')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (2, N'blue')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (3, N'black')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (4, N'white')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (5, N'brown')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (6, N'navi')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (7, N'cream')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (8, N'black-white')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (9, N'red-brown')
INSERT [dbo].[color] ([colorId], [colorValue]) VALUES (10, N'silver')
SET IDENTITY_INSERT [dbo].[color] OFF
GO
SET IDENTITY_INSERT [dbo].[comment] ON 

INSERT [dbo].[comment] ([commentId], [accountId], [shopProductId], [created_at], [updated_at], [content]) VALUES (1, 2, 4, CAST(N'2024-06-25' AS Date), NULL, N'')
SET IDENTITY_INSERT [dbo].[comment] OFF
GO
SET IDENTITY_INSERT [dbo].[discount] ON 

INSERT [dbo].[discount] ([discountId], [shopProductId], [discountValue], [startDate], [endDate], [promotionalPrice]) VALUES (1, 9, 10, CAST(N'2024-07-16T13:19:25.830' AS DateTime), CAST(N'2024-07-18T00:00:00.000' AS DateTime), 495000.0000)
INSERT [dbo].[discount] ([discountId], [shopProductId], [discountValue], [startDate], [endDate], [promotionalPrice]) VALUES (2, 9, 10, CAST(N'2024-07-26T00:00:00.000' AS DateTime), CAST(N'2024-07-28T00:00:00.000' AS DateTime), 495000.0000)
SET IDENTITY_INSERT [dbo].[discount] OFF
GO
SET IDENTITY_INSERT [dbo].[images] ON 

INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (1, 1, N'images/Nike Air Force 1 Low x Louis Vuitton.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (2, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image1.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (3, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image2.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (4, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image3.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (5, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image5.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (6, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image6.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (7, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image7.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (8, 1, N'images/Nike Air Force 1 Low x Louis Vuitton_image8.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (9, 2, N'images/Adidas Superstar 3 colors Basic.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (10, 2, N'images/Adidas Superstar 3 colors Basic_image1.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (11, 2, N'images/Adidas Superstar 3 colors Basic_image2.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (12, 2, N'images/Adidas Superstar 3 colors Basic_image3.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (13, 2, N'images/Adidas Superstar 3 colors Basic_image4.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (14, 3, N'images/Nike Air Force 1 All White.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (15, 3, N'images/Nike Air Force 1 All White_image1.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (16, 3, N'images/Nike Air Force 1 All White_image2.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (17, 3, N'images/Nike Air Force 1 All White_image3.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (18, 4, N'images/Nike Air Force 1 All White_image4.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (19, 4, N'images/Nike Air Force 1 All White_image5.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (20, 4, N'images/Nike Air Force 1 All White_image6.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (21, 6, N'images/Penny Loafer Nubuck Lemans.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (22, 6, N'images/Penny Loafer Nubuck Lemans_image1.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (23, 6, N'images/Penny Loafer Nubuck Lemans_image2.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (24, 6, N'images/Penny Loafer Nubuck Lemans_image3.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (25, 7, N'images/Chunky PENNY LOAFER White LEMANS.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (26, 7, N'images/Chunky PENNY LOAFER White LEMANS_image1.jpg')
INSERT [dbo].[images] ([imagesId], [shopProductId], [imageLink]) VALUES (27, 7, N'images/Chunky PENNY LOAFER White LEMANS_image2.jpg')
SET IDENTITY_INSERT [dbo].[images] OFF
GO
SET IDENTITY_INSERT [dbo].[message] ON 

INSERT [dbo].[message] ([messageId], [chatId], [sentTime], [senderId], [content]) VALUES (1, 1, CAST(N'2024-07-20T00:53:29.780' AS DateTime), 2, N'alo shop')
INSERT [dbo].[message] ([messageId], [chatId], [sentTime], [senderId], [content]) VALUES (2, 1, CAST(N'2024-07-20T00:53:45.230' AS DateTime), 2, N'goe a diu nao ?')
INSERT [dbo].[message] ([messageId], [chatId], [sentTime], [senderId], [content]) VALUES (3, 1, CAST(N'2024-07-25T23:08:15.553' AS DateTime), 2, N'images/Adidas Running Pureboost 22 H.RDY ''Core Black'' HQ3982_images1.jpg')
SET IDENTITY_INSERT [dbo].[message] OFF
GO
SET IDENTITY_INSERT [dbo].[order] ON 

INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (18, 4, 3850000.0000, NULL, N'Nghệ an', NULL, CAST(N'2024-06-16T00:00:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (19, 4, 8250000.0000, NULL, NULL, NULL, CAST(N'2024-06-16T00:00:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (20, 4, 0.0000, NULL, NULL, NULL, CAST(N'2024-06-16T00:00:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (21, 4, 540000.0000, NULL, NULL, NULL, CAST(N'2024-06-16T00:00:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (22, 4, 2750000.0000, NULL, NULL, NULL, CAST(N'2024-06-16T00:00:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (24, 4, 0.0000, NULL, NULL, NULL, CAST(N'2024-06-16T18:40:00.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (25, 1, 0.0000, NULL, NULL, NULL, CAST(N'2024-06-16T21:23:51.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (26, 1, 2150000.0000, N'Tran Huu Viet Hoang', N'vinh-nghe an', N'0943561200', CAST(N'2024-06-17T15:51:10.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (27, 1, 2100000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-06-17T16:14:28.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (28, 1, 2750000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-06-29T16:12:46.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (29, 1, 980000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-10T15:22:33.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (32, 1, 2730000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-11T20:56:05.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (33, 1, 0.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-11T21:01:03.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (34, 1, 0.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-11T21:13:40.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (35, 1, 0.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-11T21:15:36.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (36, 1, 530000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-13T18:45:30.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (37, 1, 990000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-14T02:46:20.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (38, 4, 4790000.0000, N'hoang', N'vinh', N'0948481235', CAST(N'2024-07-14T02:50:02.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (39, 4, 1600000.0000, N'hoang', N'vinh', N'0948481235', CAST(N'2024-07-14T02:51:59.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (40, 1, 1070000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-14T17:57:39.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (41, 1, 550000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-14T18:16:09.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (42, 1, 1085000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-16T16:05:49.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (43, 1, 1130000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-26T20:05:42.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (44, 1, 0.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-26T20:09:04.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (45, 1, 465000.0000, N'hoang', N'vinh', N'0943561200', CAST(N'2024-07-26T23:05:26.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (46, 1, 30000.0000, N'Viet hoang', N'vinh', N'0943561200', CAST(N'2024-07-27T00:20:03.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (47, 1, 0.0000, N'Viet hoang', N'vinh', N'0943561200', CAST(N'2024-07-27T00:21:38.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (48, 1, 0.0000, N'Viet hoang', N'vinh', N'0943561200', CAST(N'2024-07-27T00:22:22.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (49, 1, 540000.0000, N'Viet hoang', N'vinh', N'0943561200', CAST(N'2024-07-27T00:23:01.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (50, 1, 2750000.0000, N'Viet hoang', N'vinh', N'0943561200', CAST(N'2024-07-27T00:28:54.000' AS DateTime))
INSERT [dbo].[order] ([orderId], [userId], [totalMoney], [name], [address], [phone], [orderDate]) VALUES (51, 5, 1090000.0000, N'tran huu viet hoang', N'vinh', N'0988888888', CAST(N'2024-07-27T22:55:14.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[order] OFF
GO
SET IDENTITY_INSERT [dbo].[orderDetail] ON 

INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (25, 18, 1, 2750000.0000, 2, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (26, 18, 1, 1100000.0000, 25, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (27, 18, 1, 540000.0000, 34, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (28, 18, 1, 540000.0000, 33, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (29, 19, 1, 2750000.0000, 2, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (30, 19, 1, 2750000.0000, 7, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (31, 19, 1, 2750000.0000, 9, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (32, 20, 1, 540000.0000, 35, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (33, 21, 1, 540000.0000, 34, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (34, 22, 1, 2750000.0000, 1, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (35, 24, 1, 1700000.0000, 98, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (36, 25, 1, 2100000.0000, 14, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (37, 25, 1, 1100000.0000, 25, 4, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (38, 26, 1, 500000.0000, 39, 2, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (39, 26, 1, 550000.0000, 28, 3, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (40, 26, 1, 1100000.0000, 25, 2, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (41, 27, 1, 2100000.0000, 15, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (42, 28, 1, 2750000.0000, 2, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (43, 29, 1, 500000.0000, 39, 1, NULL, 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (44, 29, 1, 540000.0000, 35, 1, NULL, 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (45, 32, 1, 2750000.0000, 1, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (46, 33, 1, 540000.0000, 31, 4, N's?n ph?m không c?n thi?t', 4)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (47, 34, 1, 2750000.0000, 1, 4, N's?n ph?m không c?n thi?t', 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (48, 35, 1, 2750000.0000, 1, 4, N's?n ph?m không c?n thi?t', NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (49, 36, 1, 540000.0000, 31, 1, NULL, 4)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (50, 36, 1, 550000.0000, 44, 4, N'Tôi d?t th?a', NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (51, 37, 1, 500000.0000, 39, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (52, 37, 1, 550000.0000, 45, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (53, 38, 1, 2750000.0000, 2, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (54, 38, 1, 2100000.0000, 15, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (55, 39, 1, 540000.0000, 34, 1, NULL, 4)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (56, 39, 1, 1100000.0000, 25, 1, NULL, 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (57, 40, 1, 1100000.0000, 24, 1, NULL, 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (58, 41, 1, 550000.0000, 29, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (59, 42, 1, 1085000.0000, 27, 1, NULL, 1)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (60, 43, 1, 550000.0000, 28, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (61, 43, 1, 550000.0000, 29, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (62, 43, 1, 520000.0000, 44, 4, N's?n ph?m không c?n thi?t', 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (63, 44, 1, 520000.0000, 42, 4, N'tôi mu?n ch?n s?n ph?m khác', 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (64, 45, 1, 465000.0000, 42, 3, NULL, 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (65, 46, 1, 465000.0000, 46, 4, N's?n ph?m không c?n thi?t', 2)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (66, 47, 1, 465000.0000, 44, 4, N's?n ph?m không c?n thi?t', 3)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (67, 48, 1, 550000.0000, 30, 4, N's?n ph?m không c?n thi?t', NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (68, 49, 1, 2750000.0000, 2, 4, N's?n ph?m không c?n thi?t', NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (69, 49, 1, 540000.0000, 30, 1, NULL, 4)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (70, 50, 1, 2750000.0000, 1, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (71, 50, 1, 550000.0000, 28, 4, N's?n ph?m không c?n thi?t', NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (72, 51, 1, 550000.0000, 28, 1, NULL, NULL)
INSERT [dbo].[orderDetail] ([orderDetaiId], [orderId], [quantity], [price], [productItemId], [statusId], [description], [voucherId]) VALUES (73, 51, 1, 540000.0000, 34, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[orderDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[productItem] ON 

INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (1, 1, 1, 5, 7)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (2, 1, 2, 5, 6)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (3, 1, 3, 5, 3)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (4, 1, 4, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (5, 1, 5, 5, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (6, 1, 6, 5, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (7, 1, 3, 6, 7)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (8, 1, 4, 6, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (9, 1, 5, 6, 29)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (10, 2, 3, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (11, 2, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (12, 2, 5, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (13, 2, 6, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (14, 2, 3, 4, 0)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (15, 2, 5, 4, 8)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (16, 2, 7, 4, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (17, 2, 8, 4, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (18, 2, 1, 7, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (19, 2, 2, 7, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (20, 2, 3, 7, 9)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (21, 2, 4, 7, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (22, 2, 7, 7, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (23, 2, 8, 7, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (24, 3, 1, 4, 4)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (25, 3, 3, 4, 4)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (26, 3, 4, 4, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (27, 3, 6, 4, 19)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (28, 4, 3, 4, 36)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (29, 4, 4, 4, 26)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (30, 4, 6, 4, 19)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (31, 6, 3, 4, 14)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (32, 6, 5, 4, 17)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (33, 6, 6, 4, 29)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (34, 7, 4, 8, 6)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (35, 7, 5, 8, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (36, 7, 6, 8, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (37, 7, 7, 8, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (38, 8, 3, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (39, 8, 4, 5, 16)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (40, 8, 5, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (41, 8, 6, 5, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (42, 9, 3, 9, 9)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (43, 9, 4, 9, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (44, 9, 2, 9, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (45, 9, 5, 9, 19)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (46, 9, 6, 9, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (47, 9, 7, 9, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (48, 10, 2, 9, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (49, 10, 3, 9, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (50, 10, 4, 9, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (51, 10, 5, 9, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (52, 10, 6, 9, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (53, 10, 7, 9, 50)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (54, 11, 3, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (55, 11, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (56, 11, 5, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (57, 11, 6, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (58, 11, 7, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (59, 11, 8, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (60, 12, 3, 10, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (61, 12, 4, 10, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (62, 12, 5, 10, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (63, 12, 6, 10, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (64, 12, 7, 10, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (65, 12, 8, 10, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (66, 13, 3, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (67, 13, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (68, 13, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (69, 13, 6, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (70, 13, 3, 4, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (72, 13, 4, 4, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (73, 13, 5, 4, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (74, 13, 6, 4, 40)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (75, 14, 3, 10, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (76, 14, 4, 10, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (77, 14, 5, 10, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (78, 14, 6, 10, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (79, 14, 7, 10, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (80, 15, 3, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (81, 15, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (82, 15, 5, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (83, 15, 6, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (85, 15, 7, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (86, 16, 1, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (87, 16, 2, 5, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (88, 16, 3, 5, 15)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (89, 16, 4, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (90, 16, 5, 5, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (91, 16, 6, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (92, 17, 2, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (93, 17, 3, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (94, 17, 4, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (95, 17, 5, 5, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (96, 17, 6, 5, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (97, 18, 3, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (98, 18, 4, 3, 9)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (99, 18, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (100, 18, 6, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (101, 18, 7, 3, 15)
GO
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (102, 19, 3, 8, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (103, 19, 4, 8, 35)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (104, 19, 5, 8, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (105, 19, 6, 8, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (106, 19, 7, 8, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (107, 20, 3, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (108, 20, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (109, 20, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (110, 20, 6, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (111, 20, 7, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (112, 20, 8, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (113, 23, 3, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (114, 23, 4, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (115, 23, 5, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (116, 23, 6, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (117, 23, 7, 3, 15)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (118, 23, 8, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (119, 24, 1, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (120, 24, 2, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (121, 24, 3, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (122, 24, 4, 3, 15)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (123, 24, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (124, 24, 6, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (125, 25, 1, 3, 5)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (126, 25, 2, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (127, 25, 3, 3, 15)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (128, 25, 4, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (129, 25, 5, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (130, 25, 6, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (131, 25, 7, 3, 25)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (132, 26, 1, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (133, 26, 2, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (134, 26, 3, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (135, 26, 4, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (136, 26, 5, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (137, 26, 7, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (138, 26, 8, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (139, 27, 2, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (140, 27, 3, 3, 30)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (141, 27, 4, 3, 50)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (142, 27, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (143, 27, 6, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (144, 27, 7, 3, 15)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (145, 28, 2, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (146, 28, 3, 3, 10)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (147, 28, 4, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (148, 28, 5, 3, 20)
INSERT [dbo].[productItem] ([productItemId], [shopProductId], [sizeId], [colorId], [quantity]) VALUES (149, 28, 6, 3, 20)
SET IDENTITY_INSERT [dbo].[productItem] OFF
GO
SET IDENTITY_INSERT [dbo].[productLine] ON 

INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (1, N'Nike Air Force 1 Low x Louis Vuitton', 1, 1)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (2, N'Adidas Superstar', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (3, N'Nike Air Force 1 All White', 1, 1)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (4, N'Penny Loafer Nubuck Lemans', 3, 5)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (5, N'Chunky Penny Loafer White Lemans', 3, 5)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (6, N'Adidas Samba Wales Bonner', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (7, N'Adidas Samba OG TAL ''Better Scarlet'' IG8905Adidas Samba OG TAL ''Better Scarlet'' IG8905', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (8, N'Adidas Samba ''Wales Bonner Pony Tonal Core Black'' IE0580', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (9, N'Adidas Samba ''Wales Bonner Silver'' IG8181', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (10, N'Nike SB Dunk Low Steam Puppet the Year of the Dragon', 1, 1)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (11, N'Adidas Running Pureboost 22 H.RDY ''Core Black'' HQ3982', 1, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (12, N'Men’s Timberland® Premium 6-Inch Waterproof Boot TB01006163', 2, 6)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (13, N'Newmarket II Boat Chukka Dk Brown Full Grain TB0A65ZF27Newmarket II Boat Chukka Dk Brown Full Grain TB0A65ZF27', 2, 6)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (14, N'THE WOLF Minimal Chelsea Boot - Black', 2, 7)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (15, N'Adidas Adilette 22 Slides', 4, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (16, N'Buckle Sandal Lemans', 4, 5)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (18, N'THE GENT WOLF MONKSTRAP - BLACK', 3, 7)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (19, N'THEWOLF R&E MULE SANDAL - BLACK', 4, 7)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (20, N'LEMANS - LS05', 4, 5)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (21, N'Epsom LEMANS - LS01', 4, 5)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (22, N'ADILETTE SHOWER SLIDES', 5, 2)
INSERT [dbo].[productLine] ([productLineId], [productLineName], [categoryId], [brandId]) VALUES (23, N'SUPERSTAR MULE SHOES', 5, 2)
SET IDENTITY_INSERT [dbo].[productLine] OFF
GO
SET IDENTITY_INSERT [dbo].[rating] ON 

INSERT [dbo].[rating] ([ratingId], [shopProductId], [userId], [created_at], [updated_at], [starRating], [orderDetailId], [commentId]) VALUES (1, 4, 1, CAST(N'2024-06-25T20:43:10.233' AS DateTime), NULL, 5, 39, 1)
SET IDENTITY_INSERT [dbo].[rating] OFF
GO
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([roleId], [roleName]) VALUES (1, N'admin')
INSERT [dbo].[role] ([roleId], [roleName]) VALUES (2, N'user')
INSERT [dbo].[role] ([roleId], [roleName]) VALUES (3, N'shop')
SET IDENTITY_INSERT [dbo].[role] OFF
GO
SET IDENTITY_INSERT [dbo].[shop] ON 

INSERT [dbo].[shop] ([shopId], [shopName], [address], [image], [accountBalance], [accountId]) VALUES (1, N'shop1', N'FPT Hòa lạc', N'images/logo-lemans.png', 0.0000, 3)
INSERT [dbo].[shop] ([shopId], [shopName], [address], [image], [accountBalance], [accountId]) VALUES (5, N'shop2', N'FPT Hòa lạc', N'', 0.0000, 4)
INSERT [dbo].[shop] ([shopId], [shopName], [address], [image], [accountBalance], [accountId]) VALUES (6, N'shop3', N'FPT', NULL, 0.0000, 6)
INSERT [dbo].[shop] ([shopId], [shopName], [address], [image], [accountBalance], [accountId]) VALUES (7, N'shop4', N'FPT', NULL, 0.0000, 7)
INSERT [dbo].[shop] ([shopId], [shopName], [address], [image], [accountBalance], [accountId]) VALUES (8, N'shop5', N'FPT', NULL, 0.0000, 8)
SET IDENTITY_INSERT [dbo].[shop] OFF
GO
SET IDENTITY_INSERT [dbo].[shopProduct] ON 

INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (1, 1, 1, 2750000.0000, N'[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box', CAST(N'2024-05-19T00:00:00.000' AS DateTime), NULL, 0, N'images/Nike Air Force 1 Low x Louis Vuitton.jpg', N'[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (2, 1, 2, 2100000.0000, N'[Hàng Chính Hãng] Giày Thể Thao Adidas Superstar 3 Màu Basic Hottrend Full Size 36-44', CAST(N'2024-05-19T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Superstar 3 colors Basic.jpg', N'[Hàng Chính Hãng] Giày Thể Thao Adidas Superstar 3 Màu Basic Hottrend Full Size 36-44')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (3, 1, 3, 1100000.0000, N'[ Cam Kết Chính Hãng ] Giày_Nike Air Force 1 All White nam nữ Fullbox', CAST(N'2024-05-19T00:00:00.000' AS DateTime), NULL, 0, N'images/Nike Air Force 1 All White.jpg', N'[ Cam Kết Chính Hãng ] Giày_Nike Air Force 1 All White nam nữ Fullbox')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (4, 5, 3, 550000.0000, N'[like auth]Giày_Nike Air Force 1 All White nam nữ Fullbox', CAST(N'2024-05-23T00:00:00.000' AS DateTime), NULL, 0, N'images/Nike Air Force 1 All White_image6.jpg', N'[like auth]Giày_Nike Air Force 1 All White nam nữ Fullbox')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (6, 5, 4, 540000.0000, N'Giày Penny Loafer Nubuck Lemans - Bảo hành chính hãng 24 tháng', CAST(N'2024-05-23T00:00:00.000' AS DateTime), NULL, 0, N'images/Penny Loafer Nubuck Lemans.jpg', N'Giày Penny Loafer Nubuck Lemans - Bảo hành chính hãng 24 tháng')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (7, 5, 5, 540000.0000, N'The Classic Chunky PENNY LOAFER White LEMANS -BST độc quyền, da bò nguyên tấm đế cao 5cm Bảo hành chính hãng 24 tháng', CAST(N'2024-05-23T00:00:00.000' AS DateTime), NULL, 0, N'images/Chunky PENNY LOAFER White LEMANS.jpg', N'The Classic Chunky PENNY LOAFER White LEMANS -BST độc quyền, da bò nguyên tấm đế cao 5cm Bảo hành chính hãng 24 tháng')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (8, 1, 6, 500000.0000, N'Giay dep rep 1:1', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Samba Wales Bonner.jpg', N'Adidas Samba Wales Bonner')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (9, 1, 7, 550000.0000, N'Giay dep rep 1:1', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Samba OG TAL Better Scarlet IG8905.jpg', N'Adidas Samba OG TAL Better Scarlet IG8905Adidas Samba OG TAL Better Scarlet IG8905')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (10, 5, 7, 1000000.0000, N'Real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Samba OG TAL Better Scarlet IG8905.jpg', N'Adidas Samba OG TAL Better Scarle IG8905Adidas Samba OG TAL Better Scarlet IG8905')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (11, 5, 8, 700000.0000, N'rep 1:1', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Samba Wales Bonner Pony Tonal Core Black IE0580.jpg', N'Adidas Samba Wales Bonner Pony Tonal Core Black IE0580')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (12, 5, 9, 600000.0000, N'rep 1:1', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Samba Wales Bonner Silver IG8181.jpg', N'Adidas Samba Wales Bonner Silver IG8181')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (13, 6, 2, 2000000.0000, N'[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Nike Air Force 1 Low x Louis Vuitton.jpg', N'[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton Hottrend 2023 Full Box[Hàng Chính Hãng] Giày_Nike Air Force 1 Low x Louis Vuitton')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (14, 6, 10, 300000.0000, N'rep 1:1', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Nike SB Dunk Low Steam Puppet the Year of the Dragon.jpg', N'Nike SB Dunk Low Steam Puppet the Year of the Dragon')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (15, 6, 11, 578000.0000, N'giay dep', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Running Pureboost 22 H.RDY Core Black HQ3982.jpg', N'Adidas Running Pureboost 22 H.RDY Core Black HQ3982')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (16, 6, 12, 700000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Men’s Timberland® Premium 6-Inch Waterproof Boot TB01006163.jpg', N'Men’s Timberland® Premium 6-Inch Waterproof Boot TB01006163')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (17, 7, 13, 1200000.0000, N'giay real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Newmarket II Boat Chukka Dk Brown Full Grain TB0A65ZF27.jpg', N'Newmarket II Boat Chukka Dk Brown Full Grain TB0A65ZF27Newmarket II Boat Chukka Dk Brown Full Grain TB0A65ZF27')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (18, 7, 14, 1700000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/THE WOLF Minimal Chelsea Boot - Black.jpg', N'THE WOLF Minimal Chelsea Boot - Black')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (19, 7, 15, 300000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adidas Adilette 22 Slides.jpg', N'Adidas Adilette 22 Slides')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (20, 7, 16, 340000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Buckle Sandal Lemans.jpg', N'Buckle Sandal Lemans')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (23, 8, 18, 900000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/THE GENT WOLF MONKSTRAP - BLACK.jpg', N'THE GENT WOLF MONKSTRAP - BLACK')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (24, 8, 19, 420000.0000, N'real vcl', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/THEWOLF R&E MULE SANDAL - BLACK.jpg', N'THEWOLF R&E MULE SANDAL - BLACK')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (25, 8, 20, 200000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/LEMANS - LS05.jpg', N'LEMANS - LS05')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (26, 8, 21, 220000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Epsom LEMANS - LS01.jpg', N'Epsom LEMANS - LS01')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (27, 8, 22, 230000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Adilette_Shower_Slides_Black_II0018_01_standard.jpg', N'Adilette Shower Slides Black')
INSERT [dbo].[shopProduct] ([id], [shopId], [productLineId], [price], [description], [created_at], [updated_at], [is_deleted], [image], [title]) VALUES (28, 8, 23, 230000.0000, N'real', CAST(N'2024-06-08T00:00:00.000' AS DateTime), NULL, 0, N'images/Superstar_Mule_Shoes_Green_IE0758_01_standard.jpg', N'Superstar Mule Shoes')
SET IDENTITY_INSERT [dbo].[shopProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[size] ON 

INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (1, 35)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (2, 36)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (3, 37)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (4, 38)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (5, 39)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (6, 40)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (7, 41)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (8, 42)
INSERT [dbo].[size] ([sizeId], [sizeValue]) VALUES (9, 43)
SET IDENTITY_INSERT [dbo].[size] OFF
GO
SET IDENTITY_INSERT [dbo].[status] ON 

INSERT [dbo].[status] ([statusId], [statusValue]) VALUES (1, N'wait')
INSERT [dbo].[status] ([statusId], [statusValue]) VALUES (2, N'process')
INSERT [dbo].[status] ([statusId], [statusValue]) VALUES (3, N'done')
INSERT [dbo].[status] ([statusId], [statusValue]) VALUES (4, N'cancel')
SET IDENTITY_INSERT [dbo].[status] OFF
GO
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([userId], [fullName], [address], [image], [accountBalance], [dob], [gender], [accountId]) VALUES (1, N'Viet hoang', N'vinh', N'images/logo-lemans.png', 0.0000, CAST(N'2024-07-17' AS Date), 1, 2)
INSERT [dbo].[user] ([userId], [fullName], [address], [image], [accountBalance], [dob], [gender], [accountId]) VALUES (4, N'hoang', N'vinh', NULL, 0.0000, CAST(N'2024-06-14' AS Date), 1, 5)
INSERT [dbo].[user] ([userId], [fullName], [address], [image], [accountBalance], [dob], [gender], [accountId]) VALUES (5, N'tran huu viet hoang', N'vinh', NULL, 0.0000, CAST(N'2000-12-28' AS Date), 1, 9)
SET IDENTITY_INSERT [dbo].[user] OFF
GO
SET IDENTITY_INSERT [dbo].[voucher] ON 

INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (1, N'FIRSTCODE', 15000.0000, CAST(N'2025-01-01T00:00:00.000' AS DateTime), 1, N'Giảm 15000 cho 1 sản phẩm bất kì', NULL)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (2, N'SECONDCODE', 30000.0000, CAST(N'2025-01-01T00:00:00.000' AS DateTime), 1, N'Giảm 30000 cho 1 sản phẩm bất kì', NULL)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (3, N'THIRDCODE', 30000.0000, CAST(N'2025-01-02T00:00:00.000' AS DateTime), 0, N'Giảm 30000 cho sản phẩm bất kì của shop1', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (4, N'SHOP2FCODE', 10000.0000, CAST(N'2025-01-01T00:00:00.000' AS DateTime), 0, N'Giảm 10000 cho 1 sản phẩm bất kì của shop 2', 5)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (5, N'TCODE', 100000.0000, CAST(N'2024-01-01T00:00:00.000' AS DateTime), 1, N'FREE ĐƠN < 100k', NULL)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (6, N'SHOP1SCODE', 15000.0000, CAST(N'2025-01-03T00:00:00.000' AS DateTime), 0, N'Giảm 15000 cho sản phẩm bất kì của shop', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (7, N'TEST', 1000.0000, CAST(N'2024-07-13T00:00:00.000' AS DateTime), 0, N'test thu', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (8, N'test2', 1000.0000, CAST(N'2024-07-26T00:00:00.000' AS DateTime), 0, N'test', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (9, N'hoang', 1000.0000, CAST(N'2024-07-14T00:00:00.000' AS DateTime), 0, N'hoang', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (10, N'matest', 10000.0000, CAST(N'2024-07-17T00:00:00.000' AS DateTime), 0, N'ko co', 1)
INSERT [dbo].[voucher] ([voucherId], [code], [reducedAmount], [endDate], [isGlobal], [description], [shopId]) VALUES (11, N'test3', 10000.0000, CAST(N'2024-07-28T00:00:00.000' AS DateTime), 0, N'test', 1)
SET IDENTITY_INSERT [dbo].[voucher] OFF
GO
SET IDENTITY_INSERT [dbo].[voucherUser] ON 

INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (1, 1, 1, 49, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (2, 2, 1, 46, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (3, 3, 1, 47, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (4, 4, 1, 49, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (5, 5, 1, 1, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (6, 3, 4, 48, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (7, 4, 4, 49, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (8, 7, 4, 2, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (9, 7, 1, 2, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (10, 6, 4, 6, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (11, 6, 1, 1, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (12, 8, 1, 10, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (13, 8, 4, 1, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (14, 10, 1, 1, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (15, 10, 4, 1, 0)
INSERT [dbo].[voucherUser] ([voucherUserId], [voucherId], [userId], [quantity], [isUsed]) VALUES (16, 11, 1, 10, 0)
SET IDENTITY_INSERT [dbo].[voucherUser] OFF
GO
SET IDENTITY_INSERT [dbo].[wishlist] ON 

INSERT [dbo].[wishlist] ([wishListId], [userId], [shopProductId], [createdAt]) VALUES (1, 1, 1, CAST(N'2024-07-14T23:36:35.860' AS DateTime))
INSERT [dbo].[wishlist] ([wishListId], [userId], [shopProductId], [createdAt]) VALUES (3, 1, 4, CAST(N'2024-07-14T23:36:58.093' AS DateTime))
SET IDENTITY_INSERT [dbo].[wishlist] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__account__F3DBC57264B91DB3]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[account] ADD UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__chatBox__15C65EB3764824FA]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[chatBox] ADD UNIQUE NONCLUSTERED 
(
	[userId] ASC,
	[shopId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UC_ProductItem_ShopProduct_Color_Size]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[productItem] ADD  CONSTRAINT [UC_ProductItem_ShopProduct_Color_Size] UNIQUE NONCLUSTERED 
(
	[shopProductId] ASC,
	[colorId] ASC,
	[sizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__shop__F267251F2254E054]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[shop] ADD UNIQUE NONCLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__shop__F267251F534B6553]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[shop] ADD UNIQUE NONCLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__user__F267251F16DDF26F]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[user] ADD UNIQUE NONCLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__user__F267251F514BA185]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[user] ADD UNIQUE NONCLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__voucher__357D4CF919F28EB9]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[voucher] ADD UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UC_Wishlist_User_ShopProduct]    Script Date: 28/07/2024 12:30:43 SA ******/
ALTER TABLE [dbo].[wishlist] ADD  CONSTRAINT [UC_Wishlist_User_ShopProduct] UNIQUE NONCLUSTERED 
(
	[userId] ASC,
	[shopProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[account] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[account] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[account] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[comment] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[comment] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[discount] ADD  DEFAULT (getdate()) FOR [startDate]
GO
ALTER TABLE [dbo].[message] ADD  DEFAULT (getdate()) FOR [sentTime]
GO
ALTER TABLE [dbo].[order] ADD  DEFAULT (getdate()) FOR [orderDate]
GO
ALTER TABLE [dbo].[rating] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[rating] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[shop] ADD  DEFAULT ((0)) FOR [accountBalance]
GO
ALTER TABLE [dbo].[shopProduct] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[shopProduct] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[shopProduct] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [accountBalance]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((0)) FOR [gender]
GO
ALTER TABLE [dbo].[voucher] ADD  DEFAULT ('false') FOR [isGlobal]
GO
ALTER TABLE [dbo].[voucherUser] ADD  DEFAULT ('false') FOR [isUsed]
GO
ALTER TABLE [dbo].[wishlist] ADD  DEFAULT (getdate()) FOR [createdAt]
GO
ALTER TABLE [dbo].[account]  WITH CHECK ADD FOREIGN KEY([roleId])
REFERENCES [dbo].[role] ([roleId])
GO
ALTER TABLE [dbo].[chatBox]  WITH CHECK ADD FOREIGN KEY([shopId])
REFERENCES [dbo].[shop] ([shopId])
GO
ALTER TABLE [dbo].[chatBox]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[user] ([userId])
GO
ALTER TABLE [dbo].[comment]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[discount]  WITH CHECK ADD FOREIGN KEY([shopProductId])
REFERENCES [dbo].[shopProduct] ([id])
GO
ALTER TABLE [dbo].[images]  WITH CHECK ADD FOREIGN KEY([shopProductId])
REFERENCES [dbo].[shopProduct] ([id])
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD FOREIGN KEY([chatId])
REFERENCES [dbo].[chatBox] ([chatId])
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD FOREIGN KEY([senderId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[order]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[user] ([userId])
GO
ALTER TABLE [dbo].[orderDetail]  WITH CHECK ADD FOREIGN KEY([orderId])
REFERENCES [dbo].[order] ([orderId])
GO
ALTER TABLE [dbo].[orderDetail]  WITH CHECK ADD FOREIGN KEY([productItemId])
REFERENCES [dbo].[productItem] ([productItemId])
GO
ALTER TABLE [dbo].[orderDetail]  WITH CHECK ADD FOREIGN KEY([voucherId])
REFERENCES [dbo].[voucher] ([voucherId])
GO
ALTER TABLE [dbo].[orderDetail]  WITH CHECK ADD  CONSTRAINT [FK_order_detail_statusId] FOREIGN KEY([statusId])
REFERENCES [dbo].[status] ([statusId])
GO
ALTER TABLE [dbo].[orderDetail] CHECK CONSTRAINT [FK_order_detail_statusId]
GO
ALTER TABLE [dbo].[productItem]  WITH CHECK ADD FOREIGN KEY([colorId])
REFERENCES [dbo].[color] ([colorId])
GO
ALTER TABLE [dbo].[productItem]  WITH CHECK ADD FOREIGN KEY([shopProductId])
REFERENCES [dbo].[shopProduct] ([id])
GO
ALTER TABLE [dbo].[productItem]  WITH CHECK ADD FOREIGN KEY([sizeId])
REFERENCES [dbo].[size] ([sizeId])
GO
ALTER TABLE [dbo].[productLine]  WITH CHECK ADD FOREIGN KEY([brandId])
REFERENCES [dbo].[brand] ([brandId])
GO
ALTER TABLE [dbo].[productLine]  WITH CHECK ADD FOREIGN KEY([categoryId])
REFERENCES [dbo].[category] ([categoryId])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD FOREIGN KEY([shopProductId])
REFERENCES [dbo].[shopProduct] ([id])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[user] ([userId])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_comment] FOREIGN KEY([commentId])
REFERENCES [dbo].[comment] ([commentId])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_comment]
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_orderDetail] FOREIGN KEY([orderDetailId])
REFERENCES [dbo].[orderDetail] ([orderDetaiId])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_orderDetail]
GO
ALTER TABLE [dbo].[shop]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[shopProduct]  WITH CHECK ADD FOREIGN KEY([productLineId])
REFERENCES [dbo].[productLine] ([productLineId])
GO
ALTER TABLE [dbo].[shopProduct]  WITH CHECK ADD FOREIGN KEY([shopId])
REFERENCES [dbo].[shop] ([shopId])
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[voucher]  WITH CHECK ADD  CONSTRAINT [FK_shop] FOREIGN KEY([shopId])
REFERENCES [dbo].[shop] ([shopId])
GO
ALTER TABLE [dbo].[voucher] CHECK CONSTRAINT [FK_shop]
GO
ALTER TABLE [dbo].[voucherUser]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[user] ([userId])
GO
ALTER TABLE [dbo].[voucherUser]  WITH CHECK ADD FOREIGN KEY([voucherId])
REFERENCES [dbo].[voucher] ([voucherId])
GO
ALTER TABLE [dbo].[wishlist]  WITH CHECK ADD FOREIGN KEY([shopProductId])
REFERENCES [dbo].[shopProduct] ([id])
GO
ALTER TABLE [dbo].[wishlist]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[user] ([userId])
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD CHECK  (([starRating]>=(1) AND [starRating]<=(5)))
GO
USE [master]
GO
ALTER DATABASE [SWP] SET  READ_WRITE 
GO
