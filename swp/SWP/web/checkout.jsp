<%-- 
    Document   : checkout
    Created on : 7 Mar 2024, 00:16:37
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Check Out</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="assets/css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="assets\css\base.css">
        <link rel="stylesheet" href="assets\css\main.css">
        <link rel="stylesheet" href="assets/fonts/fontawesome-free-5.15.3-web/css/all.min.css">
        <style>
            .checkout__order {
                background-color: #f2f2f2;
                border: 1px solid #ccc;
                border-radius: 5px;
                padding: 20px;
                text-align: center;
            }

            .checkout__order-title {
                font-size: 20px;
                margin-bottom: 10px;
            }

            .checkout__order-products {
                font-weight: bold;
                margin-bottom: 10px;
            }

            .checkout__order-list {
                list-style-type: none;
                padding: 0;
            }

            .checkout__order-item {
                display: flex;
                justify-content: space-between;
                margin-bottom: 5px;
            }

            .checkout__order-item-name {
                flex: 1;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .checkout__order-item-price {
                flex: 0 0 80px;
                text-align: right;
            }

            .checkout__order-total {
                font-weight: bold;
                margin-top: 10px;
            }

            .site-btn {
                background-color: #f44336;
                color: white;
                padding: 10px 20px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                margin-top: 10px;
            }

            .error {
                color: red;
                font-size: medium;
            }

            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            input[type=number] {
                -moz-appearance: textfield;
            }
        </style>
        <style>
            /* Các kiểu CSS khác */
            .apply-voucher-button:disabled {
                background-color: green; /* Màu nền đỏ */
                color: #ffffff; /* Màu chữ trắng */
                opacity: 1; /* Giữ nguyên độ đậm */
                cursor: default; /* Con trỏ không thay đổi */
                font-size: 8px; /* Cỡ chữ nhỏ */
            }
            .cancel-voucher-button {
                font-size: 12px;
                font-weight: bold;
                color: white;
                background-color: red;
                cursor: default;

            }
            .modal-dialog {
                max-width: 800px; /* Rộng hơn để hiển thị tốt các chi tiết voucher */
            }

            .modal-content {
                border-radius: 8px; /* Bo tròn góc */
                overflow: hidden; /* Ẩn các nội dung vượt quá border */
            }

            .modal-header {
                background-color: #f44336; /* Màu nền đỏ */
                color: white; /* Chữ màu trắng */
            }

            .modal-footer {
                border-top: 1px solid #dee2e6; /* Thêm đường kẻ phân cách */
            }

            .btn-default {
                background-color: #6c757d; /* Màu nền cho nút Cancel */
                color: white; /* Chữ màu trắng */
            }

            .btn-success {
                background-color: #28a745; /* Màu nền cho nút Add */
                color: white; /* Chữ màu trắng */
            }

            /* Style cho các item Voucher */


            .flex {
                display: flex;
                align-items: center; /* Căn giữa các items theo chiều dọc */
                box-shadow: 0 2px 4px rgba(0,0,0,0.1); /* Đổ bóng nhẹ */
            }

            .bg-red-500 {
                background-color: #f44336; /* Màu nền đỏ */
            }

            .text-white {
                color: white; /* Chữ màu trắng */
            }

            .text-gray-700 {
                color: #4a5568; /* Màu chữ tối */
            }

            .text-gray-500 {
                color: #a0aec0; /* Màu chữ nhạt */
            }

            .text-center {
                text-align: center; /* Căn giữa chữ */
            }

            .font-bold {
                font-weight: bold; /* Chữ đậm */
            }

        </style>

    </style>
</head>
<header  class="header">
    <div class="grid">
        <!-- --------------------------------------------Header Navbar -->
        <nav class="header__navbar">
            <ul class="header__navbar-list">
                <li class="header__navbar-item header__navbar-item--get-qr">
                    Vào cửa hàng trên ứng dụng
                    <div class="header_qr">
                        <img src="assets\img\qr_code.png" alt="QR Code" class="header_qr-img">
                        <div class="header_qr-appdownload">
                            <a href="" class="header__qr-link">
                                <img src="assets\img\ggplay.png" alt="Google Play" class="header_qr-download-img">
                            </a>
                            <a href="" class="header__qr-link">
                                <img src="assets\img\appstore.png" alt="App Store" class="header_qr-download-img">
                            </a>
                        </div>
                    </div>
                </li>
                <li class="header__navbar-item"><span class="no_cursor">Kết nối</span>
                    <a href="" class="header__navbar-icon-link"><i class="icon_header fab fa-facebook"></i></a>
                    <a href="" class="header__navbar-icon-link"><i class="icon_header fab fa-instagram"></i></a>
                </li>
            </ul>
            <c:if test="${sessionScope.user != null}">
                <ul class="header__navbar-list">
                    <li class="header__navbar-item">
                        <a href="#" class="header__navbar-item-link">
                            <i class="icon_header far fa-bell"></i>Thông báo
                        </a>
                        <div class="header__thongbao">
                            <header class="header__thongbao-header">
                                <h3>Thông báo mới nhận</h3>
                            </header>
                            <ul class="header__thongbao-list">
                                <li class="header__thongbao-item">
                                    <a href="" class="header__thongbao-link header__thongbao-link--seen">
                                        <img src="assets\img\banphimco.jpg" alt="" class="header__thongbao-img">
                                        <div class="header__thongbao-info">
                                            <p class="header__thongbao-name">Bàn Phím Cơ Chính Hãng</p>
                                            <p class="header__thongbao-desc">Xuất xứ bên Mỹ nguyên seal</p>
                                        </div>
                                    </a>
                                </li>
                                <li class="header__thongbao-item">
                                    <a href="" class="header__thongbao-link">
                                        <img src="assets\img\chuot.jpg" alt="" class="header__thongbao-img">
                                        <div class="header__thongbao-info">
                                            <p class="header__thongbao-name">Chuột Gaming Chính Hãng</p>
                                            <p class="header__thongbao-desc">Xuất xứ bên Mỹ nguyên seal</p>
                                        </div>
                                    </a>
                                </li>
                                <li class="header__thongbao-item">
                                    <a href="" class="header__thongbao-link">
                                        <img src="assets\img\manhinh.jpg" alt="" class="header__thongbao-img">
                                        <div class="header__thongbao-info">
                                            <p class="header__thongbao-name">Màn hình Gaming Chính Hãng</p>
                                            <p class="header__thongbao-desc">Xuất xứ bên Mỹ nguyên seal</p>
                                        </div>
                                    </a>
                                </li>
                                <li class="header__thongbao-item">
                                    <a href="" class="header__thongbao-link">
                                        <img src="assets\img\mainboard.jpg" alt="" class="header__thongbao-img">
                                        <div class="header__thongbao-info">
                                            <p class="header__thongbao-name">MainBoard GIGABYTE Chính Hãng</p>
                                            <p class="header__thongbao-desc">Xuất xứ bên Mỹ nguyên seal</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>



                            <footer class="header__thongbao-footer">
                                <a href="" class="header__thongbao-footer-btn">Xem tất cả</a>
                            </footer>
                        </div>
                    </li>
                    <li class="header__navbar-item">
                        <a href="" class="header__navbar-item-link">
                            <i class="icon_header far fa-question-circle"></i>  Trợ giúp</a>
                    </li>
                    <li class="header__navbar-item header__navbar-user">
                        <c:if test="${requestScope.customer.getImage().length()==0||requestScope.customer.getImage().length()==null}">
                            <i class="fas fa-user text-gray-400 text-4xl"></i>
                        </c:if>
                        <c:if test="${requestScope.customer.getImage().length() > 0}">
                            <img class="header__navbar-img" src="${requestScope.customer.getImage()}"/>
                        </c:if>



                        <span class="header__navbar-name no_cursor"> ${sessionScope.user.username}</span>
                        <ul style="margin-top: -5px" class="header__navbar-user-menu">
                            <li class="header__navbar-user-item">
                                <a  href="updateuser" class="header__navbar-user-info">Tài khoản của tôi</a>
                            </li>
                            <li class="header__navbar-user-item">
                                <a  href="changepassword" class="header__navbar-user-info">Thay đổi mật khẩu</a>
                            </li>
                            <li class="header__navbar-user-item">
                                <a  href="listOrder" class="header__navbar-user-info">Đơn mua</a>
                            </li>
                            <li  class="header__navbar-user-item">
                                <a  href="logout" class="header__navbar-user-info speacial">Đăng xuất</a>
                            </li>
                        </ul>


                    </li>
                </c:if>

                <c:if test="${sessionScope.user == null}">
                    <li class="header__navbar-item header__navbar-user">
                        <img src="assets\img\user.png" alt="" class="header__navbar-img">
                        <a href="SignUpIn.jsp" class="header__navbar-name"> Đăng Nhập</a>
                    </li> 
                </c:if>
            </ul>
        </nav>
        <!-- --------------------------------------------End Header Navbar -->


        <!-- -----------------------------------------------Header Search -->
        <div class="header-with-search">
            <a href="home" class="header__qr-link">
                <div class="header__logo">
                    <img src="images/logohome.png" style="width: 90px;margin-left: 15px;height: 70px;margin-bottom: 0px;margin-top: 20px;" alt="" class="header__logo-img" onclick="resetFilters()">
                </div>
            </a>
            <form action="searchName" style="width: 100%">
                <div class="header__search">
                    <!-- History -->

                    <div class="header__search-input-wrap">
                        <input type="text" class="header__search-input" placeholder="Tìm kiếm sản phẩm" name="key" value="${requestScope.key}">
                        <!--                                                            <div class="header__search-history">
                                                                                        <h3 class="header__search-heading">Lịch sử tìm kiếm</h3>
                                                                                        <ul class="header__search-history-list">
                                                                                            <li class="header__search-history-item">
                                                                                                <a href="" class="history-item">Bàn phím cơ</a>
                                                                                            </li>
                                                                                            <li class="header__search-history-item">
                                                                                                <a href="" class="history-item">Chuột gaming</a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>-->
                    </div>
                    <!--End History  -->
                    <div class="header__search-btn">
                        <button type="submit" class="header__search-icon-btn fas fa-search" style="background-color: #F7452E; border: none;">

                        </button>


                    </div>
                </div>
            </form>
            <%
Cookie[] cookies = request.getCookies();
String uniqueProductCount = "0";
if (cookies != null) {
for (Cookie cookie : cookies) {
   if (cookie.getName().equals("uniqueProductCount")) {
       uniqueProductCount = cookie.getValue();
       break;
   }
}
}
            %>           
            <!-- Cart Giỏ Hàng -->
            <div class="header__cart">
                <div class="header__cart-wrap">
                    <i>
                        <a class="header__cart-icon fas fa-shopping-cart" href="showcart"></a>
                        <span class="header__cart-notice">
                            <%= uniqueProductCount %>
                        </span>
                    </i>
                </div>
            </div>

        </div>
</header>
<body>

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2 style="color: red">Thanh toán</h2>
                        <div class="breadcrumb__option">
                            <a style="color: red" href="home">Trang chủ</a>
                            <span style="color: red">Thanh toán</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            <div class="row">
                <!-- Phần nhập thông tin người mua bên trái -->
                <div class="col-lg-2">
                    <c:set var="err" value="${requestScope.selectedItems}"/>
                    <form action="checkout" method="get" id="checkoutForm">
                        <h4  class="checkout__order-title" style="color: blue;font-size: x-large; font-weight: bold; font-size: 15px">Thông tin người mua</h4>
                        <hr/>
                        <input hidden type="text" name="selectedItems" value=${err}>
                        <div class="checkout__input">
                            <p style="font-weight: bold ">Tên người mua</p>
                            <input style="color: black;" type="text" name="name" value="${requestScope.name}" required readonly>
                        </div>

                        <div class="checkout__input">
                            <p style="font-weight: bold">Địa chỉ người mua</p>
                            <input style="color: black" name="address" type="text" value="${requestScope.address}" placeholder="Địa chỉ" required class="checkout__input__add" readonly>

                        </div>                                
                        <div class="checkout__input">
                            <p style="font-weight: bold">Số điện thoại người mua</p>
                            <input style="color: black; " value="${requestScope.phone}" placeholder="Số điện thoại" type="number" name="phone" required readonly>

                        </div>
                </div>   

                <div class="col-lg-2">
                    <h4  class="checkout__order-title" style="color: green;font-size: x-large; font-weight: bold; font-size: 15px">Thông tin người nhận</h4>
                    <hr/>
                    <div class="checkout__input">
                        <p style="font-weight: bold ">Tên người nhận<span>*</span></p>
                        <input placeholder="Tên" type="text" value="${requestScope.name}" name="name1" required>
                        <span id="name1Error" class="error"></span>
                    </div>
                    <div class="checkout__input">
                        <p style="font-weight: bold ">Địa chỉ người nhận<span>*</span></p>
                        <input name="address1" type="text" value="${requestScope.address}" required placeholder="Địa chỉ" class="checkout__input__add">
                        <span id="address1Error" class="error"></span>
                    </div>                                
                    <div class="checkout__input">
                        <p style="font-weight: bold ">Số điện thoại người nhận<span>*</span></p>
                        <input placeholder="Số điện thoại" value="${requestScope.phone}" type="number" name="phone1" required>
                        <span id="phone1Error" class="error"></span>
                    </div>
                </div> 

                <!-- Phần Your Order bên phải -->
                <div class="col-lg-8">
                    <div class="checkout__order">
                        <h4 class="checkout__order-title">Đơn hàng của bạn</h4>
                        <div class="checkout__order-products" style="font-size: medium">Tổng <span>Sản phẩm</span></div>
                        <c:set var="cart" value="${requestScope.cart1}"/>
                        <c:forEach items="${cart.items}" var="item" varStatus="status">
                            <div class="checkout__order-total" style="font-size: medium; color: #004085; text-align: start;margin-left: 25px"><span> ${item.productItem.shop.shopName}</span></div>
                            <ul class="checkout__order-list">
                                <li class="checkout__order-item" style="flex-direction: column">

                                    <div>
                                        <input class="product-id" type="hidden" name="productID" value="${item.productItem.productItemId}">
                                        <input class="shop-id" type="hidden" name="shopId" value="${item.productItem.shop.shopId}"/>

                                        <input type="text" style="width: 70%" value="${item.productItem.shopProduct.title}" readonly class="checkout__order-item-name">
                                        <input type="text" name="quantity" 
                                               value="${item.quantity} x ${item.promotionalPrice >= 0 ? item.promotionalPrice : item.productItem.shopProduct.price}_VND" 
                                               readonly 
                                               class="checkout__order-item-price">

                                    </div>
                                    <div>
                                        <!-- Các nút giờ được quản lý bởi AJAX -->
                                        <button onclick="saveShopId('${item.productItem.shop.shopId}')" type="button" style=" width: 20%; height: 40px;" class="apply-voucher-button" data-toggle="modal" data-target="#addVoucher">Mã giảm giá</button>
                                        <button type="button" style="display: none; width: 10%; height: 40px" class="cancel-voucher-button" name="cancel">Hủy</button>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                        <fmt:formatNumber type="number" value="${cart.getTotalMoney()}" var="formattotal"/>
                        <div class="checkout__order-total" style="font-size: medium; color: red">Tổng tiền <span>${formattotal} VND</span></div>
                        <div>
                            <hr>
                            <h5 style="font-size: large;font-weight: bolder;">Chọn phương thức thanh toán</h5>
                            <input type="radio" checked="true" id="bankCode" name="bankCode" value="true">
                            <label style="font-size: medium;" for="bankCode">Thanh toán khi nhận hàng</label><br>
                            <input type="radio" id="bankCode" name="bankCode" value="false">
                            <label style="font-size: medium;" for="bankCode">Thanh toán online</label><br>
                            <button type="submit" class="site-btn">Thanh toán</button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
        <div id="addVoucher" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="voucherForm">
                        <div class="modal-header">                        
                            <h4 class="modal-title">Chọn Voucher</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="grid grid-cols-2 gap-4">
                            <input type="hidden" name="shopId" id="modelShopId"/>
                            <!-- Voucher Item -->
                            <c:forEach items="${requestScope.listVoucher}" var="list">
                                <c:if test="${list.isGlobal == 'true'}">
                                    <div class="voucher-item flex border border-gray-300 p-4 bg-white" style="cursor: pointer;" onclick="selectVoucher(this)" data-discount="${list.reducedAmount}">
                                        <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                            <div class="text-center mt-6">${list.code}</div>
                                        </div>
                                        <div class="w-3/4 pl-4">
                                            <div class="text-gray-700 font-bold">${list.code}-(<fmt:formatNumber value="${list.reducedAmount}"/>VND)</div>
                                            <div class="text-gray-500">${list.description}</div>
                                            <span class="bg-red-500 text-white text-xs px-2 py-1">DÀNH CHO BẠN (Số lượng: ${list.quantity})</span>
                                            <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate} + ${sessionScope.shopId}</div>
                                            <input type="radio" name="selectedCode" value="${list.code}" class="hidden-radio">
                                        </div>
                                    </div>
                                </c:if>
                            </c:forEach>
                            <hr>
                            <c:forEach items="${requestScope.listVoucher}" var="list">
                                <c:if test="${list.isGlobal == 'false'}">
                                    <div class="voucher-item-shop flex border border-gray-300 p-4 bg-white" style="cursor: pointer;" onclick="selectVoucher(this)" data-discount="${list.reducedAmount}" data-shop-id="${list.shopId}">
                                        <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                            <div class="text-center mt-6">${list.code}</div>
                                        </div>
                                        <div class="w-3/4 pl-4">
                                            <div class="text-gray-700 font-bold">${list.code}-(<fmt:formatNumber value="${list.reducedAmount}"/>VND)</div>
                                            <div class="text-gray-500">${list.description}</div>
                                            <span class="bg-red-500 text-white text-xs px-2 py-1">DÀNH CHO BẠN (Số lượng: ${list.quantity})</span>
                                            <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate}</div>
                                            <input type="radio" name="selectedCode" value="${list.code}" class="hidden-radio">
                                        </div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <input type="hidden" name="reducedAmount" id="modelReducedAmount"/>
                        <input type="hidden" name="code" id="modelCode"/>
                        <input type="hidden" name="productId" id="modalProductId">
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <button type="button" class="btn btn-success" onclick="submitVoucherForm()">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>

<script>
                                document.getElementById('checkoutForm').addEventListener('submit', function (event) {
                                    let isValid = true;

                                    // Validate Recipient Name
                                    const name1 = document.querySelector('input[name="name1"]');
                                    const name1Error = document.getElementById('name1Error');
                                    if (name1.value.trim() === '') {
                                        name1Error.textContent = 'Tên người nhận là bắt buộc';
                                        name1.classList.add('input-error');
                                        isValid = false;
                                    } else {
                                        name1Error.textContent = '';
                                        name1.classList.remove('input-error');
                                    }

                                    // Validate Recipient Address
                                    const address1 = document.querySelector('input[name="address1"]');
                                    const address1Error = document.getElementById('address1Error');
                                    if (address1.value.trim() === '') {
                                        address1Error.textContent = 'Địa chỉ giao hàng là bắt buộc';
                                        address1.classList.add('input-error');
                                        isValid = false;
                                    } else {
                                        address1Error.textContent = '';
                                        address1.classList.remove('input-error');
                                    }

                                    // Validate Recipient Phone
                                    const phone1 = document.querySelector('input[name="phone1"]');
                                    const phone1Error = document.getElementById('phone1Error');
                                    const regexPhoneNumber = /^(0[3|5|7|8|9])+([0-9]{8})\b/;
                                    if (phone1.value.trim() === '') {
                                        phone1Error.textContent = 'Số điện thoại là bắt buộc';
                                        phone1.classList.add('input-error');
                                        isValid = false;
                                    } else if (!regexPhoneNumber.test(phone1.value.trim())) {
                                        phone1Error.textContent = 'Số điện thoại không hợp lệ. Nó phải khớp với mẫu.';
                                        phone1.classList.add('input-error');
                                        isValid = false;
                                    } else {
                                        phone1Error.textContent = '';
                                        phone1.classList.remove('input-error');
                                    }

                                    // Prevent form submission if validation fails
                                    if (!isValid) {
                                        event.preventDefault();
                                    }
                                });
</script>
<script>

    $(document).ready(function () {
        $('.voucher-item').click(function () {
            var radioButton = $(this).find('input[type="radio"]');
            radioButton.prop("checked", true);
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.apply-voucher-button').click(function () {
            // Lấy ID sản phẩm từ input ẩn trong cùng một <li>
            var productId = $(this).closest('.checkout__order-item').find('.product-id').val();
            var shopId = $(this).closest('.checkout__order-item').find('.shop-id').val();
            $('#modelShopId').val(parseInt(shopId, 10));
            // Đặt ID sản phẩm vào trường ẩn trong modal
            $('#modalProductId').val(productId);
            // Hiển thị ID sản phẩm trong modal
        });
    });
</script>
<script>
    function submitVoucherForm(codeOverride) {
        var form = $('#voucherForm');
        var code = codeOverride ? codeOverride : $('#modelCode').val(); // Sử dụng giá trị từ tham số nếu có, nếu không lấy từ input ẩn
        var productId = $('#modalProductId').val();

        // Kiểm tra và in ra console các giá trị sẽ được gửi
        console.log('Selected Voucher Code:', code ? code : 'No voucher selected');
        console.log('Product ID:', productId ? productId : 'No product ID');

        if (code && productId) {
            // Chuyển hướng đến servlet với các query parameters
            window.location.href = 'updatePriceCart?code=' + encodeURIComponent(code) + '&productId=' + encodeURIComponent(productId);
        } else {
            alert('Vui lòng chọn một voucher.');
        }
    }
    function saveShopId(shopId) {
        localStorage.setItem('currentShopId', shopId);
        console.log("sid:", shopId);
        updateVoucherDisplay();
    }

    function updateVoucherDisplay() {
        const currentShopId = localStorage.getItem('currentShopId'); // Lấy shopId từ localStorage
        console.log("Current Shop ID:", currentShopId); // In ra để kiểm tra giá trị currentShopId
        const vouchers = document.querySelectorAll('.voucher-item-shop'); // Lấy tất cả các voucher có class là voucher-item-shop

        vouchers.forEach(voucher => {
            const voucherShopId = voucher.getAttribute('data-shop-id'); // Lấy shopId từ data attribute của voucher
            console.log("Voucher Shop ID:", voucherShopId); // In ra voucherShopId để kiểm tra

            if (voucherShopId !== currentShopId) {
                voucher.style.display = 'none'; // Ẩn voucher nếu không khớp
            } else {
                voucher.style.display = 'flex'; // Hiển thị voucher nếu khớp
            }
        });
    }

    document.addEventListener("DOMContentLoaded", function () {
        updateVoucherDisplay(); // Gọi hàm cập nhật hiển thị voucher khi trang tải
    });

    function selectVoucher(element) {
        var radioButton = $(element).find('.hidden-radio');
        radioButton.prop('checked', true);
        var selectedCode = radioButton.val();
        var discountAmount = $(element).data('discount'); // Đảm bảo rằng đây là cách chính xác để truy cập thuộc tính data-discount

        $('#modelCode').val(selectedCode);
        $('#modelReducedAmount').val(discountAmount);

        console.log('Selected Voucher Code:', selectedCode);
        console.log('Discount Amount:', discountAmount);
    }


    $(document).ready(function () {
        $('.apply-voucher-button').click(function () {
            // Lấy ID sản phẩm từ input ẩn trong cùng một <li>
            var productId = $(this).closest('.checkout__order-item').find('.product-id').val();
            // Đặt ID sản phẩm vào trường ẩn trong modal
            $('#modalProductId').val(productId);
            console.log('Product ID set in modal:', productId); // Kiểm tra xem ID sản phẩm có được đặt vào trường ẩn không
        });
    });
</script>

<script>
    $(document).ready(function () {
        $(".product-id").each(function () {
            var productId = $(this).val();
            var buttonContainer = $(this).closest('.checkout__order-item').find('.apply-voucher-button, .cancel-voucher-button');
            $.ajax({
                url: 'buttonstatus',
                type: 'GET',
                data: {productId: productId},
                success: function (response) {
                    if (response.showButtons) {
                        // Hiển thị nút "Mã giảm giá" bình thường và ẩn nút "Hủy"
                        buttonContainer.filter('.apply-voucher-button').show();
                        buttonContainer.filter('.cancel-voucher-button').hide();
                    } else {
                        // Thay đổi văn bản của nút "Mã giảm giá" thành mã và tiền, hiển thị nút "Hủy"
                        var discountText = response.discountCode + "-" + response.discountAmount;
                        buttonContainer.filter('.apply-voucher-button').text(discountText).prop('disabled', true).show();
                        buttonContainer.filter('.cancel-voucher-button').show();
                    }
                }
            });
            // Thêm sự kiện click cho nút "Hủy"
            buttonContainer.filter('.cancel-voucher-button').click(function () {
                $('#modelCode').val('0'); // Đặt mã giảm giá là 0
                $('#modalProductId').val(productId); // Đặt productId tương ứng
                submitVoucherForm('0'); // Gọi hàm với mã giảm giá là 0
            });
        });
    });
</script>



</html>

