<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"> 
        <style>
            body, html {
                height: 100%;
                margin: 0;
                background-color: #f5f6fa; /* Change the background color to white */
            }

            .chat {
                margin-top: auto;
                margin-bottom: auto;
            }
            .card {
                height: 500px;
                border-radius: 15px !important;
                background-color: rgba(0,0,0,0.4) !important;
            }
            .contacts_body {
                padding: 0.75rem 0 !important;
                overflow-y: auto;
                white-space: nowrap;
            }
            .msg_card_body {
                overflow-y: auto;
            }
            .card-header {
                border-radius: 15px 15px 0 0 !important;
                border-bottom: 0 !important;
            }
            .card-footer {
                border-radius: 0 0 15px 15px !important;
                border-top: 0 !important;
            }
            .container {
                align-content: center;
            }
            .search {
                border-radius: 15px 0 0 15px !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
            }
            .search:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .type_msg {
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                height: 60px !important;
                overflow-y: auto;
            }
            .type_msg:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .attach_btn {
                border-radius: 15px 0 0 15px !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .send_btn {
                border-radius: 0 15px 15px 0 !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .search_btn {
                border-radius: 0 15px 15px 0 !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .contacts {
                list-style: none;
                padding: 0;
            }
            .contacts li {
                width: 100% !important;
                padding: 5px 10px;
                margin-bottom: 15px !important;
            }
            .active {
                background-color: rgba(0,0,0,0.3);
            }
            .user_img {
                height: 70px;
                width: 70px;
                border: 1.5px solid #f5f6fa;
            }
            .user_img_msg {
                height: 40px;
                width: 40px;
                border: 1.5px solid #f5f6fa;
            }
            .img_cont {
                position: relative;
                height: 70px;
                width: 70px;
            }
            .img_cont_msg {
                height: 40px;
                width: 40px;
            }
            .online_icon {
                position: absolute;
                height: 15px;
                width: 15px;
                background-color: #4cd137;
                border-radius: 50%;
                bottom: 0.2em;
                right: 0.4em;
                border: 1.5px solid white;
            }
            .offline {
                background-color: #c23616 !important;
            }
            .user_info {
                margin-top: auto;
                margin-bottom: auto;
                margin-left: 15px;
            }
            .user_info span {
                font-size: 20px;
                color: white;
            }
            .user_info p {
                font-size: 10px;
                color: rgba(255,255,255,0.6);
            }
            .video_cam {
                margin-left: 50px;
                margin-top: 5px;
            }
            .video_cam span {
                color: white;
                font-size: 20px;
                cursor: pointer;
                margin-right: 20px;
            }
            .msg_cotainer {
                margin-top: auto;
                margin-bottom: auto;
                margin-left: 10px;
                border-radius: 25px;
                background-color: #82ccdd;
                padding: 10px;
                position: relative;
            }
            .msg_cotainer_send {
                margin-top: auto;
                margin-bottom: auto;
                margin-right: 10px;
                border-radius: 25px;
                background-color: #78e08f;
                padding: 10px;
                position: relative;
            }
            .msg_time {
                position: absolute;
                left: 0;
                bottom: -15px;
                color: rgba(255,255,255,0.5);
                font-size: 10px;
            }
            .msg_time_send {
                position: absolute;
                right: 0;
                bottom: -15px;
                color: rgba(255,255,255,0.5);
                font-size: 10px;
            }
            .msg_head {
                position: relative;
            }
            #action_menu_btn {
                position: absolute;
                right: 10px;
                top: 10px;
                color: white;
                cursor: pointer;
                font-size: 20px;
            }
            .action_menu {
                z-index: 1;
                position: absolute;
                padding: 15px 0;
                background-color: rgba(0,0,0,0.5);
                color: white;
                border-radius: 15px;
                top: 30px;
                right: 15px;
                display: none;
            }
            .action_menu ul {
                list-style: none;
                padding: 0;
                margin: 0;
            }
            .action_menu ul li {
                width: 100%;
                padding: 10px 15px;
                margin-bottom: 5px;
            }
            .action_menu ul li i {
                padding-right: 10px;
            }
            .action_menu ul li:hover {
                cursor: pointer;
                background-color: rgba(0,0,0,0.2);
            }
            @media(max-width: 576px) {
                .contacts_card {
                    margin-bottom: 15px !important;
                }
            }
            .welcome-container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                height: 100%;
                text-align: center;
            }
            .welcome-image {
                width: 150px;
                height: 150px;
                margin-bottom: 20px;
            }
            .welcome-text {
                font-size: 24px;
                font-weight: bold;
            }
            .action_menu.show {
                display: block !important;
            }
            .results-wrapper {
                background-color: #fff;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                margin: 20px 0;
            }
            .shop-container {
                display: flex;
                flex-wrap: wrap;
                gap: 10px;
            }
            .shop {
                display: flex;
                align-items: center;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 5px;
                width: 250px;
                cursor: pointer;
                transition: background-color 0.2s;
            }
            .shop:hover {
                background-color: #f0f0f0;
            }
            .shop img {
                max-width: 50px;
                height: auto;
                border-radius: 5px;
                margin-right: 10px;
            }
            .shop h3 {
                font-size: 1em;
                margin: 0;
                flex: 1;
                text-align: left;
            }
            .msg_image {
                max-width: 200px; /* Adjust the max-width as needed */
                max-height: 200px; /* Adjust the max-height as needed */
                border-radius: 5px;
                display: block;
                margin-top: 10px;
            }

        </style>
    </head>


    <body>
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100">
                <div class="col-md-4 col-xl-3 chat">
                    <div class="flex items-center mb-6">
                        <c:set value="${requestScope.userProfile}" var="user"/>
                        <c:set value="${sessionScope.user}" var="account"/>
                        <div class="w-12 h-12 bg-gray-200 rounded-full flex items-center justify-center">
                            <c:if test="${user.image.length()==0||user.image.length()==null}"> 
                                <i class="fas fa-user text-gray-400 text-4xl"></i>
                            </c:if>
                            <c:if test="${user.image.length() > 0}">
                                <img src="${user.image}" alt="img"/>
                            </c:if>
                        </div>
                        <div class="ml-4">
                            <div class="font-bold">${account.username}</div>
                        </div>
                    </div>
                    <ul>
                        <li class="mb-4">
                            <a href="updateuser" class="flex items-center text-gray-700">
                                <i class="fas fa-id-card mr-2"></i>
                                Hồ Sơ
                            </a>
                        </li>
                        <li class="mb-4">
                            <a href="changepassword" class="flex items-center text-gray-700">
                                <i class="fas fa-lock mr-2"></i>
                                Đổi Mật Khẩu
                            </a>
                        </li>
                        <li class="mb-4">
                            <a href="listOrder" class="flex items-center text-red-500">
                                <i class="fas fa-shopping-cart mr-2"></i>
                                Đơn Mua
                            </a>
                        </li>
                        <li class="mb-4">
                            <a href="userchatwithshop" class="flex items-center text-gray-700">
                                <i class="fas fa-envelope mr-2"></i>
                                Tin nhắn
                            </a>
                        </li>
                        <li class="mb-4">
                            <a href="userVoucher" class="flex items-center text-gray-700">
                                <i class="fas fa-gift mr-2"></i>
                                Kho Voucher
                            </a>
                        </li>
                        <li class="mb-4">
                            <a href="home" class="flex items-center text-gray-700">
                                <i class="fas fa-backward mr-2"></i>
                                Trở về trang chủ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-xl-3 chat">
                    <div class="card mb-sm-3 mb-md-0 contacts_card">
                        <div class="card-header">
                            <form id="searchForm" action="searchshop" method="get" class="input-group" onsubmit="return submitFormSearch()">
                                <input type="text" id="searchInput" placeholder="Search..." name="keyword" class="form-control search">
                                <div class="input-group-prepend">
                                    <button type="submit" class="input-group-text search_btn"><i class="fas fa-search"></i></button>
                                </div>
                                <div class="input-group-append">
                                    <c:if test="${not empty searchShop}">
                                        <a href="userchatwithshop" class="btn btn-danger btn-sm">x</a>
                                    </c:if>
                                </div>
                            </form>
                        </div>
                        <c:if test="${not empty searchShop}">
                            <div class="results-wrapper">
                                <div id="searchResults" class="shop-container">

                                    <c:forEach var="shop" items="${searchShop}">
                                        <div class="shop" onclick="selectShop('${shop.shopId}')">
                                            <c:if test="${shop.image.length()==0||shop.image.length()==null}">
                                                <i class="fas fa-user text-gray-400 text-4xl"></i>
                                            </c:if>
                                            <c:if test="${shop.image.length() > 0}">
                                                <img src="${shop.image}" alt="${shop.shopName}">
                                            </c:if>

                                            <h3>${shop.shopName}</h3>
                                        </div>
                                    </c:forEach>

                                </div>
                            </div>
                        </c:if>

                        <!-- list chat -->
                        <c:if test="${ empty searchShop}">
                            <div class="card-body contacts_body">
                                <ul class="contacts">
                                    <c:forEach var="listchat" items="${requestScope.listChat}">
                                        <li class="chat-item" data-chat-id="${listchat.chatId}" data-shop-id="${listchat.shop.shopId}" onclick="submitForm('${listchat.chatId}', '${listchat.shop.shopId}', this)">
                                            <input type="hidden" name="chatId" value="${requestScope.chatId}" />
                                            <div class="d-flex bd-highlight">
                                                <div class="img_cont">

                                                    <c:if test="${listchat.shop.image.length()==0||listchat.shop.image.length()==null}">
                                                        <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                    </c:if>
                                                    <c:if test="${listchat.shop.image.length() > 0}">
                                                        <img src="${listchat.shop.image}" class="rounded-circle user_img">
                                                    </c:if>
                                                    <span class="online_icon"></span>
                                                </div>
                                                <div class="user_info">
                                                    <span>${listchat.shop.shopName}</span>
                                                    
                                                </div>
                                                <span id="action_menu_btn_${listchat.chatId}" onclick="toggleActionMenu('${listchat.chatId}', event)"><i class="fas fa-ellipsis-v"></i></span>
                                                <div id="action_menu_${listchat.chatId}" class="action_menu" style="display: none;">
                                                    <ul>
                                                        <li><a href="viewshop?sid=${listchat.shop.shopId}"><i class="fas fa-user-circle"></i> Xem cửa hàng</a></li>
                                                        <li><a href="#" id="deleteChatLink_${listchat.chatId}" onclick="confirmDelete(event, '${listchat.chatId}')"><i class="fas fa-trash-alt"></i>Xoá hộp trò chuyện</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </c:if>
                        <!-- Hidden form to submit chat ID -->
                        <form id="chatForm" action="userchatwithshop" method="post">
                            <input type="hidden" id="chatIdInputChatForm" name="chatId" value="" />
                            <input type="hidden" id="shopIdInputChatForm" name="shopId" value="" />
                        </form>
                        <form id="messageForm" action="addchatuserwithshop">
                            <input type="hidden" id="chatIdInputMessageForm" name="chatId" value="" />
                            <input type="hidden" id="shopIdInputMessageForm" name="shopId" value="" />
                            <input type="hidden" name="sid" value="${requestScope.getShop.shopId}" />
                            <input type="hidden" id="messageContent" name="messageContent" value="" />
                        </form>
                        <form id="messageUpdateForm" action="updatemessage" >
                            <input type="hidden" id="messageIdInputMessageForm" name="messageId" value="" />

                            <input type="hidden" name="sid" value="${requestScope.getShop.shopId}" />
                            <input type="hidden" id="messageUdContent" name="messageUdContent" value="" />
                        </form>
                        <!-- Content chat -->
                        <div class="card-footer"></div>
                    </div>
                </div>
                <div class="col-md-8 col-xl-6 chat">
                    <div class="card">
                        <c:if test="${empty requestScope.getShop}">
                            <div class="welcome-container">
                                <img src="images\Message.jpg" alt="Welcome" class="welcome-image">
                                <div class="welcome-text">Chào mừng đến với Shoes Shop</div>
                            </div>
                        </c:if>
                        <c:if test="${not empty requestScope.getShop}">
                            <div class="card-header msg_head">
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <c:if test="${requestScope.getShop.image.length()==0||requestScope.getShop.image.length()==null}">
                                            <i class="fas fa-user text-gray-400 text-4xl"></i>
                                        </c:if>
                                        <c:if test="${requestScope.getShop.image.length() > 0}">
                                            <img src="${requestScope.getShop.image}" class="rounded-circle user_img">
                                        </c:if>

                                        <span class="online_icon"></span>
                                    </div>
                                    <div class="user_info">
                                        <span>${requestScope.getShop.shopName}</span>
                                    </div>
                                </div>



                            </div>
                            <div class="card-body msg_card_body">
                                <c:forEach var="listm" items="${requestScope.listMessage}">
                                    <c:if test="${listm.getSenderId() != sessionScope.user.getAccountId()}">
                                        <div class="d-flex justify-content-start mb-4">
                                            <div class="img_cont_msg">
                                                <c:if test="${listm.shop.image.length()==0||listm.shop.image.length()==null}">
                                                    <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                </c:if>
                                                <c:if test="${listm.shop.image.length() > 0}">
                                                    <img src="${listm.shop.image}" class="rounded-circle user_img_msg">
                                                </c:if>

                                            </div>
                                            <div class="msg_cotainer">
                                                <c:choose>
                                                    <c:when test="${listm.content.startsWith('images/')}">
                                                        <img src="${listm.content}" alt="Product Image" class="msg_image">
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${listm.content}
                                                    </c:otherwise>
                                                </c:choose>

                                                <span class="msg_time">${listm.senderTime}</span>
                                            </div>

                                        </div>
                                    </c:if>
                                    <c:if test="${listm.getSenderId() == sessionScope.user.getAccountId()}">
                                        <div class="d-flex justify-content-end mb-4">
                                            <div class="msg_cotainer_send">
                                                <c:choose>
                                                    <c:when test="${listm.content.startsWith('images/')}">
                                                        <img src="${listm.content}" alt="Product Image" class="msg_image">
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${listm.content}
                                                    </c:otherwise>
                                                </c:choose>

                                                <span class="msg_time_send">${listm.senderTime}</span>
                                            </div>
                                            <div class="img_cont_msg">
                                                <c:if test="${listm.user.image.length()==0||listm.user.image.length()==null}">
                                                    <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                </c:if>
                                                <c:if test="${listm.user.image.length() > 0}">
                                                    <img src="${listm.user.image}" class="rounded-circle user_img_msg">
                                                </c:if>

                                            </div>
                                            <span id="msg_action_menu_btn_${listm.getMessageId()}" onclick="toggleMsgActionMenu(${listm.getMessageId()})">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </span>
                                            <div id="msg_action_menu_${listm.getMessageId()}" class="action_menu">
                                                <ul>
                                                    <li><a href="javascript:void(0);" onclick="displayMessageContent(${listm.messageId}, '${listm.content}')"><i class="fas fa-edit"></i>Cập nhật tin nhắn</a></li>

                                                    <li><a href="javascript:void(0);" onclick="confirmDeleteMsg(${listm.messageId}, ${listm.shop.shopId})"><i class="fas fa-trash-alt"></i>Xóa tin nhắn</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <div class="card-footer">
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text attach_btn" onclick="document.getElementById('fileInput').click();">
                                            <i class="fas fa-paperclip"></i>
                                        </span>

                                    </div>
                                    <textarea id="messageInput" name="message" class="form-control type_msg" placeholder="Type your message..."></textarea>
                                    <div class="input-group-append">
                                        <button id="addButton" type="button" class="input-group-text send_btn" style="display: block;" onclick="submitMessage()">
                                            <i class="fas fa-location-arrow"></i>
                                        </button>
                                        <div class="input-group-append">
                                            <button id="sendButton" type="button" class="input-group-text send_btn" style="display: none;" onclick="submitUpdatedMessage()">
                                                <i class="fas fa-location-arrow"></i>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <input type="file" id="fileInput" style="display: none;" onchange="handleFileSelect(event)" />

                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script>
        function handleFileSelect(event) {
            const file = event.target.files[0];
            if (file) {
                const filePath = 'images/' + file.name;
                const messageInput = document.getElementById('messageInput');
                messageInput.value = messageInput.value + '\n' + filePath;
            }
        }
        function submitFormSearch() {
            const searchKeyword = document.getElementById('searchInput').value.trim();

            if (searchKeyword === '') {
                alert("Please enter a search term");
                return false; // Ngăn form gửi đi khi không có từ khóa
            }

            const xhr = new XMLHttpRequest();
            xhr.open("GET", "searchshop", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    console.log(xhr.responseText); // Xử lý phản hồi từ servlet
                }
            };

            xhr.send("keyword=" + encodeURIComponent(searchKeyword));


        }
        function selectShop(shopId) {

            window.location.href = 'userchatwithshop?sid=' + encodeURIComponent(shopId);
        }

        function submitForm(chatId, shopId, element) {
            localStorage.setItem('chatId', chatId);
            localStorage.setItem('shopId', shopId);
            console.log('chatId:', chatId);
            console.log('shopId ', shopId);
            document.getElementById('chatIdInputChatForm').value = chatId;
            document.getElementById('shopIdInputChatForm').value = shopId;

            // Remove active class from all chat items
            const chatItems = document.querySelectorAll('.chat-item');
            chatItems.forEach(item => item.classList.remove('active'));

            // Add active class to the clicked chat item
            element.classList.add('active');

            document.getElementById('chatForm').submit();
        }

        function submitMessage() {
            const messageContent = document.getElementById('messageInput').value.trim();

            if (messageContent === '') {
                alert('Please enter a message before sending.');
                return;
            }

            const chatId = localStorage.getItem('chatId');
            const shopId = localStorage.getItem('shopId');
            console.log('chatId: ', chatId);
            console.log('shopId: ', shopId);
            document.getElementById('chatIdInputMessageForm').value = chatId;
            document.getElementById('shopIdInputMessageForm').value = shopId;
            document.getElementById('messageContent').value = messageContent;

            // Update active class for the selected chat item
            const chatItems = document.querySelectorAll('.chat-item');
            chatItems.forEach(item => {
                if (item.getAttribute('data-chat-id') === chatId) {
                    item.classList.add('active');
                } else {
                    item.classList.remove('active');
                }
            });

            document.getElementById('messageForm').submit();
        }

        document.addEventListener('DOMContentLoaded', () => {
            const chatId = localStorage.getItem('chatId');
            if (chatId) {
                const chatItem = document.querySelector(`.chat-item[data-chat-id='${chatId}']`);
                if (chatItem) {
                    chatItem.classList.add('active');
                }
            }
        });
        function toggleActionMenu(chatId, event) {
            event.stopPropagation(); // Ngăn chặn sự kiện click lan ra ngoài
            const actionMenu = document.getElementById(`action_menu_${chatId}`);
            actionMenu.style.display = actionMenu.style.display === 'block' ? 'none' : 'block';

            // Đảm bảo chỉ hiển thị menu của một chat tại một thời điểm
            const otherMenus = document.querySelectorAll('.action_menu');
            otherMenus.forEach(menu => {
                if (menu.id !== `action_menu_${chatId}`) {
                    menu.style.display = 'none';
                }
            });
        }

        function confirmDelete(event, chatId) {
            event.preventDefault(); // Ngăn điều hướng mặc định
            event.stopPropagation(); // Ngăn chặn sự kiện click lan ra ngoài

            if (confirm("Bạn có chắc chắn muốn xóa hộp trò chuyện này không?")) {
                const deleteChatLink = document.getElementById(`deleteChatLink_${chatId}`);
                window.location.href = deleteChatLink.href; // Điều hướng đến đường link xóa chat
            } else {
                console.log("Chat deletion canceled.");
            }

            // Ẩn menu sau khi xử lý
            const actionMenu = document.getElementById(`action_menu_${chatId}`);
            actionMenu.style.display = 'none';
        }

// Khi DOMContentLoaded, đảm bảo rằng href của deleteChatLink được cập nhật
        document.addEventListener('DOMContentLoaded', () => {
            const chatId = document.getElementById('chatIdInputMessageForm').value;
            const deleteChatLinks = document.querySelectorAll("[id^='deleteChatLink_']");
            deleteChatLinks.forEach(link => {
                link.href = `deletechatbox?chatId=${chatId}`;
            });
        });

// Ngăn chặn sự kiện click trên toàn bộ tài liệu khi click vào các phần tử khác ngoài menu
        document.addEventListener('click', () => {
            const actionMenus = document.querySelectorAll('.action_menu');
            actionMenus.forEach(menu => {
                menu.style.display = 'none';
            });
        });
        function toggleMsgActionMenu(messageId) {
            const msgActionMenu = document.getElementById('msg_action_menu_' + messageId);
            if (msgActionMenu) {
                msgActionMenu.classList.toggle('show');

                // Nếu menu hiển thị, thêm sự kiện để ẩn nó khi nhấp bên ngoài
                if (msgActionMenu.classList.contains('show')) {
                    document.addEventListener('click', hideMenus, true);
                }
            } else {
                console.log('Element not found for messageId:', messageId);
            }
        }

        function hideMenus(event) {
            const actionMenus = document.querySelectorAll('.action_menu.show');
            actionMenus.forEach(menu => {
                menu.classList.remove('show');
            });

            // Loại bỏ sự kiện sau khi ẩn menu để tránh ràng buộc nhiều lần
            document.removeEventListener('click', hideMenus, true);
        }

// Ngăn chặn việc ẩn menu khi nhấp vào chính nó hoặc nút bật
        document.addEventListener('click', function (event) {
            if (event.target.closest('.action_menu') || event.target.closest('[id^="action_menu_btn_"], [id^="msg_action_menu_btn_"]')) {
                event.stopPropagation();
            }
        });

        // Hàm xác nhận trước khi xóa tin nhắn
        function confirmDeleteMsg(messageId, shopId) {
            const confirmation = confirm("Bạn có chắc chắn muốn xóa tin nhắn này?");
            if (confirmation) {
                window.location.href = 'deletemessage?messageId=' + messageId + '&sid=' + shopId;

            }
        }

        function displayMessageContent(messageId, content) {
            const messageInput = document.getElementById('messageInput');
            messageInput.value = content.trim(); // Hiển thị nội dung vào textarea và trim khoảng trắng

            // Cập nhật giá trị hidden input cho messageId và shopId
            document.getElementById('messageIdInputMessageForm').value = messageId;


            // Hiển thị nút "Send"
            // Ẩn nút "Send" cho việc thêm tin nhắn mới và hiển thị nút "Update"
            document.getElementById('addButton').style.display = 'none';
            document.getElementById('sendButton').style.display = 'block';
        }

        function submitUpdatedMessage() {
            const messageUdContent = document.getElementById('messageInput').value.trim();
            const messageId = document.getElementById('messageIdInputMessageForm').value;


            if (messageUdContent === '') {
                alert('Please enter a message before sending.');
                return;
            }

            document.getElementById('messageUdContent').value = messageUdContent;

            // Gửi form để cập nhật tin nhắn
            document.getElementById('messageUpdateForm').action = 'updatemessage';
            document.getElementById('messageUpdateForm').submit();
        }

    </script>




</html>
