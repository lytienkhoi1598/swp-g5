<%-- 
    Document   : ChangePassShop
    Created on : 26 Jul 2024, 11:45:46
    Author     : admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <title>Change Password</title>
        <style>
            .form-control {
                width: 100%;
                padding: 0.75rem;
                border: 1px solid #cbd5e0;
                border-radius: 0.25rem;
                font-size: 1rem;
                color: #4a5568;
                transition: border-color 0.2s, box-shadow 0.2s;
            }

            .form-control:focus {
                border-color: #63b3ed;
                box-shadow: 0 0 0 3px rgba(99, 179, 237, 0.5);
                outline: none;
            }

            .btn-primary {
                background-color: #3182ce;
                color: #fff;
                padding: 0.75rem 1.25rem;
                border: none;
                border-radius: 0.25rem;
                font-size: 1rem;
                cursor: pointer;
                transition: background-color 0.2s;
            }

            .btn-primary:hover {
                background-color: #2b6cb0;
            }

            .input-lg {
                font-size: 1.25rem;
                padding: 0.75rem 1.25rem;
                height: auto;
            }
        </style>
    </head>
    <body class="bg-gray-100">

        <div class="mx-auto mt-10">

            <div class="flex">
                <!-- Sidebar -->
                <jsp:include page="menu.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="w-3/4 bg-white p-6 rounded shadow ml-6">
                    <h2 class="text-2xl font-bold mb-4">Đổi Mật Khẩu</h2>
                    <p class="text-gray-600 mb-6">Cập nhật mật khẩu của bạn để bảo mật tài khoản</p>

                    <form action="changepassword" method="post">
                        <input type="hidden" name="user" value="${sessionScope.user.username}"/>

                        <div class="mb-4">
                            <input type="password" class="input-lg form-control" name="oldpass" id="password1" placeholder="Mật khẩu cũ" required>
                        </div>

                        <div class="mb-4">
                            <input type="password" class="input-lg form-control" name="pass" id="password1" placeholder="Mật khẩu mới" required>
                        </div>

                        <div class="mb-4">
                            <input type="password" class="input-lg form-control" name="cfpass" id="password2" placeholder="Nhập lại mật khẩu" required>
                        </div>

                        <h5 class="text-red-500 mb-4">${requestScope.err}</h5>

                        <div class="flex justify-start">
                            <input type="submit" class="btn btn-primary btn-load btn-lg" data-loading-text="Đang thay đổi mật khẩu..." value="Thay đổi mật khẩu">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

