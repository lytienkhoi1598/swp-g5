<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
        <style>
            body, html {
                height: 100%;
                margin: 0;
                background-color: #f5f6fa; /* Change the background color to white */
            }

            .chat {
                margin-top: auto;
                margin-bottom: auto;
            }
            .card {
                height: 500px;
                border-radius: 15px !important;
                background-color: rgba(0,0,0,0.4) !important;
            }
            .contacts_body {
                padding: 0.75rem 0 !important;
                overflow-y: auto;
                white-space: nowrap;

            }
            .msg_card_body {
                overflow-y: auto;
            }
            .card-header {
                border-radius: 15px 15px 0 0 !important;
                border-bottom: 0 !important;
            }
            .card-footer {
                border-radius: 0 0 15px 15px !important;
                border-top: 0 !important;
            }
            .container {
                align-content: center;
            }
            .search {
                border-radius: 15px 0 0 15px !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
            }
            .search:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .type_msg {
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                height: 60px !important;
                overflow-y: auto;
            }
            .type_msg:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .attach_btn {
                border-radius: 15px 0 0 15px !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .send_btn {
                border-radius: 0 15px 15px 0 !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .search_btn {
                border-radius: 0 15px 15px 0 !important;
                background-color: rgba(0,0,0,0.3) !important;
                border: 0 !important;
                color: white !important;
                cursor: pointer;
            }
            .contacts {
                list-style: none;
                padding: 0;
            }
            .contacts li {
                width: 100% !important;
                padding: 5px 10px;
                margin-bottom: 15px !important;
            }
            .active {
                background-color: rgba(0,0,0,0.3);
            }
            .user_img {
                height: 70px;
                width: 70px;
                border: 1.5px solid #f5f6fa;
            }
            .user_img_msg {
                height: 40px;
                width: 40px;
                border: 1.5px solid #f5f6fa;
            }
            .img_cont {
                position: relative;
                height: 70px;
                width: 70px;
            }
            .img_cont_msg {
                height: 40px;
                width: 40px;
            }
            .online_icon {
                position: absolute;
                height: 15px;
                width: 15px;
                background-color: #4cd137;
                border-radius: 50%;
                bottom: 0.2em;
                right: 0.4em;
                border: 1.5px solid white;
            }
            .offline {
                background-color: #c23616 !important;
            }
            .user_info {

                margin-top: auto;
                margin-bottom: auto;
                margin-left: 15px;
            }
            .user_info span {
                font-size: 20px;
                color: white;
            }
            .user_info p {
                font-size: 10px;
                color: rgba(255,255,255,0.6);
            }
            .video_cam {
                margin-left: 50px;
                margin-top: 5px;
            }
            .video_cam span {
                color: white;
                font-size: 20px;
                cursor: pointer;
                margin-right: 20px;
            }
            .msg_cotainer {
                margin-top: auto;
                margin-bottom: auto;
                margin-left: 10px;
                border-radius: 25px;
                background-color: #82ccdd;
                padding: 10px;
                position: relative;
            }
            .msg_cotainer_send {
                margin-top: auto;
                margin-bottom: auto;
                margin-right: 10px;
                border-radius: 25px;
                background-color: #78e08f;
                padding: 10px;
                position: relative;
            }
            .msg_time {
                position: absolute;
                left: 0;
                bottom: -15px;
                color: rgba(255,255,255,0.5);
                font-size: 10px;
            }
            .msg_time_send {
                position: absolute;
                right: 0;
                bottom: -15px;
                color: rgba(255,255,255,0.5);
                font-size: 10px;
            }
            .msg_head {
                position: relative;
            }
            #action_menu_btn {
                position: absolute;
                right: 10px;
                top: 10px;
                color: white;
                cursor: pointer;
                font-size: 20px;
            }
            .action_menu {
                z-index: 1;
                position: absolute;
                padding: 15px 0;
                background-color: rgba(0,0,0,0.5);
                color: white;
                border-radius: 15px;
                top: 30px;
                right: 15px;
                display: none;
            }
            .action_menu ul {
                list-style: none;
                padding: 0;
                margin: 0;
            }
            .action_menu ul li {
                width: 100%;
                padding: 10px 15px;
                margin-bottom: 5px;
            }
            .action_menu ul li i {
                padding-right: 10px;
            }
            .action_menu ul li:hover {
                cursor: pointer;
                background-color: rgba(0,0,0,0.2);
            }
            @media(max-width: 576px) {
                .contacts_card {
                    margin-bottom: 15px !important;
                }
            }
            .welcome-container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                height: 100%;
                text-align: center;
            }
            .welcome-image {
                width: 150px;
                height: 150px;
                margin-bottom: 20px;
            }
            .welcome-text {
                font-size: 24px;
                font-weight: bold;
            }
            .action_menu.show {
                display: block !important;
            }
            .results-wrapper {
                background-color: #fff;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                margin: 20px 0;
            }
            .shop-container {
                display: flex;
                flex-wrap: wrap;
                gap: 10px;
            }
            .shop {
                display: flex;
                align-items: center;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 5px;
                width: 250px;
                cursor: pointer;
                transition: background-color 0.2s;
            }
            .shop:hover {
                background-color: #f0f0f0;
            }
            .shop img {
                max-width: 50px;
                height: auto;
                border-radius: 5px;
                margin-right: 10px;
            }
            .shop h3 {
                font-size: 1em;
                margin: 0;
                flex: 1;
                text-align: left;
            }
            .msg_image {
                max-width: 200px; /* Adjust the max-width as needed */
                max-height: 200px; /* Adjust the max-height as needed */
                border-radius: 5px;
                display: block;
                margin-top: 10px;

            </style>
        </head>


        <body style="background-color: white;">
            <div class="mx-auto mt-10">
                <div class="flex">
                    <jsp:include page="menu.jsp"></jsp:include>
                        <div class="col-md-4 col-xl-3 chat">
                            <div class="card mb-sm-3 mb-md-0 contacts_card">
                                <div class="card-header">
                                    <form id="searchForm" action="searchuser" method="get" class="input-group" onsubmit="return submitFormSearch()">
                                        <input type="text" id="searchInput" placeholder="Search..." name="keyword" class="form-control search">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="input-group-text search_btn"><i class="fas fa-search"></i></button>
                                        </div>
                                        <div class="input-group-append">
                                        <c:if test="${not empty searchUser}">
                                            <a href="shopchatwithuser" class="btn btn-danger btn-sm">x</a>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <c:if test="${not empty searchUser}">
                                <div class="results-wrapper">
                                    <div id="searchResults" class="shop-container">

                                        <c:forEach var="user" items="${searchUser}">
                                            <div class="shop" onclick="selectUser('${user.userId}')">
                                                <c:if test="${user.image.length()==0||user.image.length()==null}">
                                                    <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                </c:if>
                                                <c:if test="${user.image.length() > 0}">
                                                    <img src="${user.image}" alt="${user.fullName}">
                                                </c:if>

                                                <h3>${user.fullName}</h3>
                                            </div>
                                        </c:forEach>

                                    </div>
                                </div>
                            </c:if>

                            <!-- list chat -->
                            <c:if test="${ empty searchUser}">
                                <div class="card-body contacts_body">
                                    <ul class="contacts">
                                        <c:forEach var="listchat" items="${requestScope.listChat}">
                                            <li class="chat-item" data-chat-id="${listchat.chatId}" data-shop-id="${listchat.shop.shopId}" onclick="submitForm('${listchat.chatId}', '${listchat.user.userId}', this)">
                                                <input type="hidden" name="chatId" value="${requestScope.chatId}" />
                                                <div class="d-flex bd-highlight">
                                                    <div class="img_cont">

                                                        <c:if test="${listchat.user.image.length()==0||listchat.user.image.length()==null}">
                                                            <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                        </c:if>
                                                        <c:if test="${listchat.user.image.length() > 0}">
                                                            <img src="${listchat.user.image}" class="rounded-circle user_img">
                                                        </c:if>
                                                        <span class="online_icon"></span>
                                                    </div>
                                                    <div class="user_info">
                                                        <span>${listchat.user.fullName}</span>
                                                        
                                                    </div>
                                                    <span id="action_menu_btn_${listchat.chatId}" onclick="toggleActionMenu('${listchat.chatId}', event)"><i class="fas fa-ellipsis-v"></i></span>
                                                    <div id="action_menu_${listchat.chatId}" class="action_menu" style="display: none;">
                                                        <ul>

                                                            <li><a href="#" id="deleteChatLink_${listchat.chatId}" onclick="confirmDelete(event, '${listchat.chatId}')"><i class="fas fa-trash-alt"></i>Xóa hộp trò chuyện</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </c:if>


                            <!-- Hidden form to submit chat ID -->
                            <form id="chatForm" action="shopchatwithuser" method="post">
                                <input type="hidden" id="chatIdInputChatForm" name="chatId" value="" />
                                <input type="hidden" id="userIdInputChatForm" name="userId" value="" />
                            </form>
                            <form id="messageForm" action="addchatshopwithuser">
                                <input type="hidden" id="chatIdInputMessageForm" name="chatId" value="" />

                                <input type="hidden" name="uid" value="${requestScope.getUser.userId}" />
                                <input type="hidden" id="messageContent" name="messageContent" value="" />
                            </form>
                            <form id="messageUpdateForm" action="updatemessage" method="post">
                                <input type="hidden" id="messageIdInputMessageForm" name="messageId" value="" />

                                <input type="hidden" name="uid" value="${requestScope.getUser.userId}" />
                                <input type="hidden" id="messageUdContent" name="messageUdContent" value="" />
                            </form>
                            <!-- Content chat -->
                            <div class="card-footer"></div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xl-6 chat">
                        <div class="card">
                            <c:if test="${empty requestScope.getUser}">
                                <div class="welcome-container">
                                    <img src="images\Message.jpg" alt="Welcome" class="welcome-image">
                                    <div class="welcome-text">Chào mừng đến với Shoes Shop</div>

                                </div>
                            </c:if>
                            <c:if test="${not empty requestScope.getUser}">
                                <div class="card-header msg_head">
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <c:if test="${requestScope.getUser.image.length()==0||requestScope.getUser.image.length()==null}">
                                                <i class="fas fa-user text-gray-400 text-4xl"></i>
                                            </c:if>
                                            <c:if test="${requestScope.getUser.image.length() > 0}">
                                                <img src="${requestScope.getUser.image}" class="rounded-circle user_img">
                                            </c:if>

                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <span>${requestScope.getUser.fullName}</span>
                                        </div>
                                    </div>



                                </div>
                                <div class="card-body msg_card_body">
                                    <c:forEach var="listm" items="${requestScope.listMessage}">
                                        <c:if test="${listm.getSenderId() != sessionScope.user.getAccountId()}">
                                            <div class="d-flex justify-content-start mb-4">
                                                <div class="img_cont_msg">
                                                    <c:if test="${listm.user.image.length()==0||listm.user.image.length()==null}">
                                                        <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                    </c:if>
                                                    <c:if test="${listm.user.image.length() > 0}">
                                                        <img src="${listm.user.image}" class="rounded-circle user_img_msg">
                                                    </c:if>

                                                </div>
                                                <div class="msg_cotainer">
                                                    <c:choose>
                                                        <c:when test="${listm.content.startsWith('images/')}">
                                                            <img src="${listm.content}" alt="Product Image" class="msg_image">
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${listm.content}
                                                        </c:otherwise>
                                                    </c:choose>

                                                    <span class="msg_time">${listm.senderTime}</span>
                                                </div>

                                            </div>
                                        </c:if>
                                        <c:if test="${listm.getSenderId() == sessionScope.user.getAccountId()}">
                                            <div class="d-flex justify-content-end mb-4">
                                                <div class="msg_cotainer_send">
                                                    <c:choose>
                                                        <c:when test="${listm.content.startsWith('images/')}">
                                                            <img src="${listm.content}" alt="Product Image" class="msg_image">
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${listm.content}
                                                        </c:otherwise>
                                                    </c:choose>

                                                    <span class="msg_time_send">${listm.senderTime}</span>
                                                </div>
                                                <div class="img_cont_msg">
                                                    <c:if test="${listm.shop.image.length()==0||listm.shop.image.length()==null}">
                                                        <i class="fas fa-user text-gray-400 text-4xl"></i>
                                                    </c:if>
                                                    <c:if test="${listm.shop.image.length() > 0}">
                                                        <img src="${listm.shop.image}" class="rounded-circle user_img_msg">
                                                    </c:if>

                                                </div>
                                                <span id="msg_action_menu_btn_${listm.getMessageId()}" onclick="toggleMsgActionMenu(${listm.getMessageId()})">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </span>
                                                <div id="msg_action_menu_${listm.getMessageId()}" class="action_menu">
                                                    <ul>
                                                        <li><a href="javascript:void(0);" onclick="displayMessageContent(${listm.messageId}, '${listm.content}')"><i class="fas fa-edit"></i>Cập nhật tin nhắn</a></li>

                                                        <li><a href="javascript:void(0);" onclick="confirmDeleteMsg(${listm.messageId}, ${listm.user.userId})"><i class="fas fa-trash-alt"></i>Xóa tin nhắn</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                <div class="card-footer">
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text attach_btn" onclick="document.getElementById('fileInput').click();">
                                                <i class="fas fa-paperclip"></i>
                                            </span>

                                        </div>
                                        <textarea id="messageInput" name="message" class="form-control type_msg" placeholder="Type your message..."></textarea>
                                        <div class="input-group-append">
                                            <button id="addButton" type="button" class="input-group-text send_btn" style="display: block;" onclick="submitMessage()">
                                                <i class="fas fa-location-arrow"></i>
                                            </button>
                                            <div class="input-group-append">
                                                <button id="sendButton" type="button" class="input-group-text send_btn" style="display: none;" onclick="submitUpdatedMessage()">
                                                    <i class="fas fa-location-arrow"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="file" id="fileInput" style="display: none;" onchange="handleFileSelect(event)" />
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        <script>
            function handleFileSelect(event) {
                const file = event.target.files[0];
                if (file) {
                    const filePath = 'images/' + file.name;
                    const messageInput = document.getElementById('messageInput');
                    messageInput.value = messageInput.value + '\n' + filePath;
                }
            }
            function submitFormSearch() {
                const searchKeyword = document.getElementById('searchInput').value.trim();

                if (searchKeyword === '') {
                    alert("Please enter a search term");
                    return false; // Ngăn form gửi đi khi không có từ khóa
                }

                const xhr = new XMLHttpRequest();
                xhr.open("GET", "searchuser", true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        console.log(xhr.responseText); // Xử lý phản hồi từ servlet
                    }
                };

                xhr.send("keyword=" + encodeURIComponent(searchKeyword));


            }
            function selectUser(userId) {

                window.location.href = 'shopchatwithuser?uid=' + encodeURIComponent(userId);
            }

            function submitForm(chatId, userId, element) {
                localStorage.setItem('chatId', chatId);
                localStorage.setItem('userId', userId);
                document.getElementById('chatIdInputChatForm').value = chatId;
                document.getElementById('userIdInputChatForm').value = userId;

                // Remove active class from all chat items
                const chatItems = document.querySelectorAll('.chat-item');
                chatItems.forEach(item => item.classList.remove('active'));

                // Add active class to the clicked chat item
                element.classList.add('active');

                document.getElementById('chatForm').submit();
            }

            function submitMessage() {
                const messageContent = document.getElementById('messageInput').value.trim();

                if (messageContent === '') {
                    alert('Please enter a message before sending.');
                    return;
                }

                const chatId = localStorage.getItem('chatId');


                document.getElementById('chatIdInputMessageForm').value = chatId;

                document.getElementById('messageContent').value = messageContent;

                // Update active class for the selected chat item
                const chatItems = document.querySelectorAll('.chat-item');
                chatItems.forEach(item => {
                    if (item.getAttribute('data-chat-id') === chatId) {
                        item.classList.add('active');
                    } else {
                        item.classList.remove('active');
                    }
                });

                document.getElementById('messageForm').submit();
            }

            document.addEventListener('DOMContentLoaded', () => {
                const chatId = localStorage.getItem('chatId');
                if (chatId) {
                    const chatItem = document.querySelector(`.chat-item[data-chat-id='${chatId}']`);
                    if (chatItem) {
                        chatItem.classList.add('active');
                    }
                }
            });
            function toggleActionMenu(chatId, event) {
                event.stopPropagation();
                var actionMenu = document.getElementById('action_menu_' + chatId);
                actionMenu.style.display = (actionMenu.style.display === 'none' || actionMenu.style.display === '') ? 'block' : 'none';
            }
            function confirmDelete(event, chatId) {
                event.preventDefault();
                if (confirm("Bạn có chắc chắn muốn xóa hộp trò chuyện này không?")) {
                    var form = document.createElement('form');
                    form.method = 'POST';
                    form.action = 'deletechatbox'; // Replace with your servlet's URL

                    var inputChatId = document.createElement('input');
                    inputChatId.type = 'hidden';
                    inputChatId.name = 'chatId';
                    inputChatId.value = chatId;

                    form.appendChild(inputChatId);
                    document.body.appendChild(form);
                    form.submit();
                }
            }

            // Hide the action menu when clicking anywhere else on the document
            document.addEventListener('click', function (event) {
                var actionMenus = document.querySelectorAll('.action_menu');
                actionMenus.forEach(function (menu) {
                    menu.style.display = 'none';
                });
            });

            // Prevent the action menu from closing when clicking inside it
            document.querySelectorAll('.action_menu').forEach(function (menu) {
                menu.addEventListener('click', function (event) {
                    event.stopPropagation();
                });
            });
            function toggleMsgActionMenu(messageId) {
                const msgActionMenu = document.getElementById('msg_action_menu_' + messageId);
                if (msgActionMenu) {
                    msgActionMenu.classList.toggle('show');

                    // Nếu menu hiển thị, thêm sự kiện để ẩn nó khi nhấp bên ngoài
                    if (msgActionMenu.classList.contains('show')) {
                        document.addEventListener('click', hideMenus, true);
                    }
                } else {
                    console.log('Element not found for messageId:', messageId);
                }
            }

            function hideMenus(event) {
                const actionMenus = document.querySelectorAll('.action_menu.show');
                actionMenus.forEach(menu => {
                    menu.classList.remove('show');
                });

                // Loại bỏ sự kiện sau khi ẩn menu để tránh ràng buộc nhiều lần
                document.removeEventListener('click', hideMenus, true);
            }

            // Ngăn chặn việc ẩn menu khi nhấp vào chính nó hoặc nút bật
            document.addEventListener('click', function (event) {
                if (event.target.closest('.action_menu') || event.target.closest('[id^="action_menu_btn_"], [id^="msg_action_menu_btn_"]')) {
                    event.stopPropagation();
                }
            });

            // Hàm xác nhận trước khi xóa tin nhắn
            function confirmDeleteMsg(messageId, userId) {
                const confirmation = confirm("Bạn có chắc chắn muốn xóa tin nhắn này?");
                if (confirmation) {
                    window.location.href = 'deletemessage?messageId=' + messageId + '&uid=' + userId;

                }
            }

            function displayMessageContent(messageId, content) {
                const messageInput = document.getElementById('messageInput');
                messageInput.value = content.trim(); // Hiển thị nội dung vào textarea và trim khoảng trắng

                // Cập nhật giá trị hidden input cho messageId và shopId
                document.getElementById('messageIdInputMessageForm').value = messageId;


                // Hiển thị nút "Send"
                // Ẩn nút "Send" cho việc thêm tin nhắn mới và hiển thị nút "Update"
                document.getElementById('addButton').style.display = 'none';
                document.getElementById('sendButton').style.display = 'block';
            }

            function submitUpdatedMessage() {
                const messageUdContent = document.getElementById('messageInput').value.trim();
                const messageId = document.getElementById('messageIdInputMessageForm').value;


                if (messageUdContent === '') {
                    alert('Please enter a message before sending.');
                    return;
                }

                document.getElementById('messageUdContent').value = messageUdContent;

                // Gửi form để cập nhật tin nhắn
                document.getElementById('messageUpdateForm').action = 'updatemessage';
                document.getElementById('messageUpdateForm').submit();
            }

        </script>




    </html>
