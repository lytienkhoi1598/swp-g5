<%-- 
    Document   : VoucherDetailShop
    Created on : Jul 13, 2024, 7:50:43 PM
    Author     : VIET HOANG
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        /* CSS để ẩn các mũi tên của input type="number" */
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance: textfield;
        }
        .modal-header {
            background-color: #f8f9fa;
            border-bottom: 1px solid #dee2e6;
            padding: 1rem 1.5rem;
        }
        .modal-title {
            margin: 0;
            line-height: 1.5;
            font-size: 1.25rem;
        }
        .modal-body {
            padding: 1.5rem;
        }
        .modal-footer {
            background-color: #f8f9fa;
            border-top: 1px solid #dee2e6;
            padding: 1rem;
        }
        .btn-custom {
            background-color: #007bff;
            color: white;
        }
        .btn-custom:hover {
            background-color: #0056b3;
            color: white;
        }
        .voucher-card {
            border: 2px dashed #e0e0e0;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0,0,0,0.1);
            transition: transform 0.2s ease;
            width: 300px;
            margin: 20px auto; /* Căn giữa thẻ voucher */
        }
        .voucher-card:hover {
            transform: scale(1.02);
        }
        .voucher-header {
            background-color: #ff5a5f;
            color: white;
            padding: 15px;
            text-align: center;
            font-size: 1.25rem;
        }
        .voucher-body {
            padding: 15px;
            background-color: white;
        }
        .voucher-code {
            font-size: 1.25rem;
            font-weight: bold;
            color: #333;
        }
        .voucher-description {
            margin-top: 10px;
            color: #666;
        }
        .voucher-expiry {
            margin-top: 10px;
            font-size: 0.875rem;
            color: #999;
        }
        .container-center {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        
    </style>
    <script>
        // JavaScript để vô hiệu hóa việc thay đổi giá trị từ các phím mũi tên
        function disableArrowKeys(event) {
            if (event.key === "ArrowUp" || event.key === "ArrowDown") {
                event.preventDefault();
            }
        }
        function showVoucherModal() {
            $('#VforUser').modal('show');
        }
    </script>

    <body class="bg-gray-100">
        <div class="mx-auto mt-10">
            <div class="flex">
                <jsp:include page="menu.jsp"></jsp:include>
                    <div class="w-3/4 p-6">
                        <div class="grid grid-cols-2 gap-1">
                            <!-- Voucher Item -->
                        <c:set value="${requestScope.voucher}" var="list" />
                        <div class="voucher-card">
                            <div class="voucher-header">
                                ${list.code}
                            </div>
                            <div class="voucher-body">
                                <div class="voucher-code">
                                    ${list.code}
                                </div>
                                <div class="voucher-description">
                                    ${list.description}
                                </div>
                                <div class="voucher-expiry">
                                    Có hiệu lực đến: ${list.endDate}
                                </div>
                            </div>
                        </div>
                        <form action="updateVoucher" method="post" class="w-full bg-white p-4">
                            <div class="mb-4">
                                <label for="code" class="block text-gray-700">Mã Voucher</label>
                                <input type="text" id="code" name="code" value="${list.code}" class="border border-gray-300 p-2 w-full" readonly>
                            </div>
                            <div class="mb-4">
                                <label for="description" class="block text-gray-700">Mô tả</label>
                                <input type="text" id="description" name="description" value="${list.description}" class="border border-gray-300 p-2 w-full" required>
                                <p style="color: red">${requestScope.err}</p>
                            </div>
                            <div class="mb-4">
                                <label for="reducedAmount" class="block text-gray-700">Số tiền giảm</label>
                                <fmt:formatNumber type="number" pattern="######" value="${list.reducedAmount}" var="formattotal"/>
                                <input type="number" onkeydown="disableArrowKeys(event)" id="reducedAmount" name="reducedAmount" value="${formattotal}" min="1" required class="border border-gray-300 p-2 w-full">
                            </div>
                            <div class="mb-4">
                                <label for="endDate" class="block text-gray-700">Ngày hết hạn</label>
                                <input type="date" id="endDate" name="endDate" value="${list.endDate}" class="border border-gray-300 p-2 w-full">
                            </div>
                            <div>
                                <button type="submit" class="bg-red-500 text-white p-2">Cập nhật</button>
                                <button type="button"id="giveVoucherBtn" class="bg-red-500 text-white p-2" onclick="showVoucherModal()">Tặng cho khách hàng</button>
                                <span id="voucherStatus" class="text-gray-500" style="display: none;">Voucher đã quá hạn</span>
                            </div>

                            <p style="color: green">${requestScope.done}</p>
                        </form>
                    </div>         
                </div>
            </div>
            <div id="VforUser" class="modal fade hidden">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="addVoucherUser" method="post">
                            <div class="modal-header">						
                                <h4 class="modal-title">Chọn người dùng để tặng Voucher</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">	
                                <table class="custom-table table">
                                    <thead style="font-size: 12px;">
                                    <input type="hidden" name="code" value="${list.code}"/>
                                    <tr>
                                        <th>Chọn tất cả <input type="checkbox" checked="true" id="selectAll" onclick="toggleSelectAll()"/></th>
                                        <th>Ảnh đại diện</th>
                                        <th>Tên khách hàng</th>
                                        <th>Tên tài khoản</th>
                                        <th>Số voucher tặng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.listU}" var="user">
                                            <tr>
                                                <td>
                                                    <input class="user-checkbox" type="checkbox" checked="true" onclick="updateHiddenInput(this, 'user-input-${user.userId}')"/>
                                                    <input type="hidden" name="userId" id="user-input-${user.userId}" value="${user.userId}"/>
                                                </td>
                                                <td><img style="width: 30px; height: 30px" src="${user.image}" alt="alt"/></td>
                                                <td>${user.fullName}</td>
                                                <td>${user.username}</td>
                                                <td><input type="number" name="quantity" min="1" value="1" required onkeydown="disableArrowKeys(event)"/></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>

                            </div>

                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy">
                                <input type="submit"id="submitBtn" class="btn btn-success" value="Tặng">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </body>
    <style>
        .custom-table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0;
            font-size: 14px;
        }
        .custom-table thead {
            background-color: #f8f9fa;
        }
        .custom-table thead th {
            border-bottom: 2px solid #dee2e6;
            padding: 10px;
            text-align: left;
        }
        .custom-table tbody tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }
        .custom-table tbody td {
            padding: 10px;
            border-bottom: 1px solid #dee2e6;
        }
        .custom-table img {
            border-radius: 50%;
            object-fit: cover;
        }
        .custom-table input[type="number"] {
            width: 80px;
            padding: 5px;
            border: 1px solid #ced4da;
            border-radius: 4px;
        }
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const endDateInput = document.getElementById('endDate');
            const giveVoucherButton = document.getElementById('giveVoucherBtn');
            const voucherStatus = document.getElementById('voucherStatus');

            function checkEndDate() {
                const endDate = new Date(endDateInput.value);
                const today = new Date();
                today.setHours(0, 0, 0, 0);  // Thiết lập thời gian về đầu ngày để chỉ so sánh ngày

                // Vô hiệu hóa nút và hiển thị thông báo nếu ngày hết hạn nhỏ hơn ngày hiện tại
                if (endDate < today) {
                    giveVoucherButton.disabled = true;
                    giveVoucherButton.classList.add('disabled-button');
                    voucherStatus.style.display = 'inline'; // Hiển thị thông báo
                } else {
                    giveVoucherButton.disabled = false;
                    giveVoucherButton.classList.remove('disabled-button');
                    voucherStatus.style.display = 'none'; // Ẩn thông báo
                }
            }

            // Kiểm tra ngay khi trang được tải
            checkEndDate();

            // Kiểm tra mỗi khi giá trị của endDate thay đổi
            endDateInput.addEventListener('change', checkEndDate);
        });
        function checkCheckboxStatus() {
            const checkboxes = document.querySelectorAll('.user-checkbox');
            const submitBtn = document.getElementById('submitBtn');
            let checked = false;
            checkboxes.forEach((checkbox) => {
                if (checkbox.checked) {
                    checked = true;
                }
            });
            submitBtn.disabled = !checked;
        }

        // Gắn sự kiện thay đổi cho tất cả các checkbox
        document.addEventListener('DOMContentLoaded', (event) => {
            const checkboxes = document.querySelectorAll('.user-checkbox');
            checkboxes.forEach((checkbox) => {
                checkbox.addEventListener('change', checkCheckboxStatus);
            });
        });
        function toggleSelectAll() {
            const selectAll = document.getElementById('selectAll');
            const checkboxes = document.querySelectorAll('.user-checkbox');
            checkboxes.forEach((checkbox) => {
                checkbox.checked = selectAll.checked;
            });
            checkCheckboxStatus();
        }

        // JavaScript để kiểm tra xem tất cả các checkbox có được chọn hay không
        function checkAllSelected() {
            const selectAll = document.getElementById('selectAll');
            const checkboxes = document.querySelectorAll('.user-checkbox');
            selectAll.checked = Array.from(checkboxes).every(checkbox => checkbox.checked);
            checkCheckboxStatus();
        }

        // Gắn sự kiện thay đổi cho checkbox "Chọn tất cả"
        document.addEventListener('DOMContentLoaded', (event) => {
            document.getElementById('selectAll').addEventListener('change', toggleSelectAll);
            const checkboxes = document.querySelectorAll('.user-checkbox');
            checkboxes.forEach((checkbox) => {
                checkbox.addEventListener('change', checkAllSelected);
            });
        });
        function updateHiddenInput(checkbox, inputId) {
            const hiddenInput = document.getElementById(inputId);
            if (checkbox.checked) {
                hiddenInput.value = checkbox.getAttribute('data-userid'); // Sử dụng data-userid hoặc một thuộc tính tương tự
            } else {
                hiddenInput.value = 0; // Cập nhật giá trị thành 0 nếu không được chọn
            }
        }

// Thêm sự kiện submit cho form
        document.querySelector('form').addEventListener('submit', prepareFormForSubmission);
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</html>
