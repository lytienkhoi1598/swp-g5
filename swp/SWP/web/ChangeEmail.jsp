<%-- 
    Document   : clone
    Created on : May 26, 2024, 3:14:36 PM
    Author     : VIET HOANG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <title>Change Email</title>
    </head>
    <body class="bg-blue-100 flex items-center justify-center min-h-screen">
        <div class="bg-white p-8 rounded-lg shadow-md w-96">
            <div>
                <div class="flex items-center mb-4">
                    <i class="fas fa-arrow-left text-red-500"></i>
                </div>
                <h2 class="text-center text-xl font-semibold mb-2">Nhập mã xác nhận</h2>
                <p class="text-center text-gray-600 mb-4">Mã xác minh đã được gửi đến Email Mới của bạn</p>
                <form id="changeEmail" action="changeEmail" method="POST">
                    <div class="flex justify-between mb-6">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                    </div>
                    <input type="hidden" name="email" value="${sessionScope.email}">
                    <input type="hidden" name="code" value="${sessionScope.code}">
                    <input type="hidden" name="verificationCode" id="verificationCode">
                     <!-- comment -->
                    <button type="button" class="w-full bg-red-400 text-white py-3 rounded-lg" onclick="submitForm()">KẾ TIẾP</button>
                    <p>${requestScope.err}<p>
                </form>
            </div>
    </body>
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            const inputs = document.querySelectorAll('.w-12');

            inputs.forEach((input, index) => {
                input.addEventListener('input', (e) => {
                    const value = e.target.value;
                    if (value.length === 1) {
                        if (!/\d/.test(value)) {
                            e.target.value = '';
                            return;
                        }
                        if (index < inputs.length - 1) {
                            inputs[index + 1].focus();
                        }
                    }
                });

                input.addEventListener('keydown', (e) => {
                    if (e.key === 'Backspace' && input.value === '' && index > 0) {
                        inputs[index - 1].focus();
                    }
                });
            });
        });

        function submitForm() {
            const inputs = document.querySelectorAll('.w-12');
            let code = '';
            inputs.forEach(input => {
                code += input.value;
            });
            document.getElementById('verificationCode').value = code;
            document.getElementById('changeEmail').submit();
        }

       
    </script>
</html>
