<%-- 
    Document   : EnterPinCodejsp
    Created on : May 25, 2024, 8:02:14 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Enter Pin Code</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
        <link rel="stylesheet" href="assets/css/base.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/fonts/fontawesome-free-5.15.3-web/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath() %>/style/assets/css/stylePageSignIN_UP.css" />
    </head>


    <body>
        <a href="<%=request.getContextPath() %>/login" onclick="resetCountdown()">← Trở về đăng nhập</a>
        <div class="container" id="containerIn" style="width: 500px; min-height: 480px;">
            <div class="form-container sign-in-container" style="width: 100%;">
                <form action="pincode" method="post" onsubmit="return validateInput()">
                    <h1 style="padding-bottom: 40px;">Mã Pin</h1> 
                    <input type="text" class="input-lg form-control" name="pincode" id="pincode" placeholder="Mã Code" required>  
                    <h5 style="color: red"><%= request.getAttribute("err") != null ? request.getAttribute("err") : "" %></h5>
                    <div class="col-sm-12 text-center">
                        <p>Thời gian còn lại để nhập mã PIN: <span id="countdown"></span></p>
                        <a href="forgotpass" onclick="resetCountdown()">Gửi lại mã Pin</a>
                    </div>                  
                    <button type="submit" style="margin-bottom: 20px">Xác nhận mã Pin</button>
                </form>
            </div>
        </div>
    </body>



    <script>
        var countdownTime = 90;
        var countdownInterval;
        var isCountingDown = sessionStorage.getItem('isCountingDown') === 'true';

        function startCountdown() {
            var countdownElement = document.getElementById("countdown");

            var storedCountdownTime = sessionStorage.getItem('countdownTime');
            if (storedCountdownTime !== null && !isCountingDown) {
                countdownTime = parseInt(storedCountdownTime);
            }

            countdownInterval = setInterval(function () {
                var minutes = Math.floor(countdownTime / 60);
                var seconds = countdownTime % 60;

                seconds = seconds < 10 ? "0" + seconds : seconds;
                countdownElement.innerHTML = minutes + ":" + seconds;

                if (countdownTime <= 0) {
                    clearInterval(countdownInterval);
                    countdownElement.innerHTML = "Hết giờ!";
                }

                countdownTime--;
                sessionStorage.setItem('countdownTime', countdownTime.toString());
            }, 1000);

            isCountingDown = true;
            sessionStorage.setItem('isCountingDown', 'true');
        }

        function resetCountdown() {
            if (countdownInterval) {
                clearInterval(countdownInterval);
            }

            countdownTime = 90;
            sessionStorage.removeItem('countdownTime');
            startCountdown();
        }

        function validateInput() {
            var userInput = document.getElementById("pincode").value;
            var errorElement = document.getElementById("error");

            if (userInput === "") {
                errorElement.innerHTML = "Vui lòng nhập mã PIN!";
                return false;
            }

            if (!/^\d+$/.test(userInput)) {
                errorElement.innerHTML = "Vui lòng nhập số hợp lệ!";
                return false;
            }

            errorElement.innerHTML = "";
            return true;
        }

        window.onload = function () {
            if (!isCountingDown) {
                startCountdown();
            }

            var resetCountdownFlag = <%= session.getAttribute("resetCountdown") != null ? "true" : "false" %>;
            if (resetCountdownFlag) {
                resetCountdown();
                sessionStorage.removeItem('countdownTime');
        <% session.removeAttribute("resetCountdown"); %>
            }
        };
    </script>





</body>
</html>
