<%-- 
    Document   : ShopVoucher
    Created on : Jul 13, 2024, 6:54:44 PM
    Author     : VIET HOANG
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body class="bg-gray-100">
        <div class="mx-auto mt-10">
            <div class="flex">
                <jsp:include page="menu.jsp"></jsp:include>
                <p style="color: green">${requestScope.giveDone}</p>
                <div class="w-3/4 p-6">
                    <p style="color: green">${requestScope.done}</p>
                    <form action="searchVoucherShop">
                        <div class="flex items-center mb-4">
                            <input type="text" name="code" placeholder="Nhập mã voucher tại đây" class="border border-gray-300 p-2 w-1/2">
                            <button class="bg-red-500 text-white-50 p-2 ml-2">Tìm kiếm</button>
                        </div>
                    </form>
                    <div class="grid grid-cols-2 gap-4">
                        <!-- Voucher Item -->
                        <c:forEach items="${requestScope.listVoucher}" var="list">
                            <button onclick="gotoDetail('${list.code}')">
                                <div class="flex border border-gray-300 p-4 bg-white">
                                    <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                        <div class="text-center mt-6">${list.code}</div>
                                    </div>
                                    <div class="w-3/4 pl-4">
                                        <div class="text-gray-700 font-bold">${list.code}</div>
                                        <div class="text-gray-500">${list.description}</div>
                                        <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate}</div>
                                    </div>
                                </div>
                            </button>
                        </c:forEach>

                    </div>
                    <button style="width: 150px" class="bg-green-500 text-white p-2"><a href="addVoucher" style="font-size: 16px">Thêm Voucher</a></button>
                </div>         
            </div>
        </div>
    </body>
    <script>
        function gotoDetail(code) {
            var form = document.createElement('form');
            form.method = 'POST';
            form.action = 'voucherDetail';

            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'code';
            input.value = code;
            form.appendChild(input);

            document.body.appendChild(form);
            form.submit();
        }
    </script>
</html>
