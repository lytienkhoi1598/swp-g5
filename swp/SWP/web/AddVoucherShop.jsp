<%-- 
    Document   : AddVoucherShop
    Created on : Jul 13, 2024, 9:32:18 PM
    Author     : VIET HOANG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        .container {
            display: flex;
            align-items: flex-start;
        }
        .menu {
            width: 250px;
            margin-right: 20px;
        }
        .content {
            flex-grow: 1;
            background-color: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .form-control, .form-select {
            width: 100%;
            padding: 10px;
            margin: 5px 0 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        .btn {
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-right: 10px;
        }
        .btn-default {
            background-color: #ccc;
            color: #333;
        }
        .btn-success {
            background-color: #4CAF50;
            color: white;
        }
        .btn:hover {
            opacity: 0.8;
        }
        .modal-footer {
            display: flex;
            justify-content: space-between;
            border-top: 1px solid #e5e5e5;
            padding-top: 10px;
        }
        .message {
            color: red;
            margin-bottom: 10px;
        }
        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script>
        // JavaScript để vô hiệu hóa việc thay đổi giá trị từ các phím mũi tên
        function disableArrowKeys(event) {
            if (event.key === "ArrowUp" || event.key === "ArrowDown") {
                event.preventDefault();
            }
        }
    </script>
    <style>
        .form-group {
            margin-bottom: 1em;
        }
        .form-control {
            padding: 0.5em;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 100%;
        }
    </style>
    <script>
        window.onload = function() {
            // Lấy ngày hôm nay
            var today = new Date();
            var day = String(today.getDate()).padStart(2, '0');
            var month = String(today.getMonth() + 1).padStart(2, '0'); // Tháng bắt đầu từ 0
            var year = today.getFullYear();

            // Định dạng yyyy-mm-dd
            var todayDate = year + '-' + month + '-' + day;

            // Thiết lập giá trị mặc định và giá trị min cho input type="date"
            var endDateInput = document.querySelector('input[name="endDate"]');
            endDateInput.value = todayDate;
            endDateInput.min = todayDate;
        };
    </script>
    <body class="bg-gray-100">

        <div class="container mx-auto mt-10">
            <div class="menu">
                <jsp:include page="menu.jsp"></jsp:include>
                </div>
                <div class="content">
                    <h1 class="text-2xl font-bold mb-6">Thêm Voucher</h1>
                    <form action="addVoucher" method="post">
                        <div class="form-group">
                            <label>Mã</label>
                            <input name="code" type="text" class="form-control" required>
                            <p style="color: red">${requestScope.err}</p>
                    </div>
                    <div class="form-group">
                        <label>Số tiền khuyến mãi</label>
                        <input name="reducedAmount" onkeydown="disableArrowKeys(event)" min="1" type="number" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Ngày hết hạn</label>
                        <input name="endDate" type="date" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea name="description" class="form-control" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <a href="manageVoucherShop" class="btn btn-default">Quay Về</a>
                        <input type="submit" class="btn btn-success" value="Thêm">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
