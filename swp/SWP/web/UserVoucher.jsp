<%-- 
    Document   : UserVoucher
    Created on : Jul 2, 2024, 6:51:56 PM
    Author     : VIET HOANG
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
        <title>JSP Page</title>
    </head>
    <body class="bg-gray-100 font-roboto">
        <div class="flex">
            <!-- Sidebar -->
            <c:set value="${requestScope.userProfile}" var="user"/>
            <c:set value="${sessionScope.user}" var="account"/>
            <div class="w-1/4 bg-white p-4 rounded shadow">
                <div class="flex items-center mb-6">
                    <div class="w-12 h-12 bg-gray-200 rounded-full flex items-center justify-center">
                        <c:if test="${user.image.length()==0||user.image.length()==null}">
                            <i class="fas fa-user text-gray-400 text-4xl"></i>
                        </c:if>
                        <c:if test="${user.image.length() > 0}">
                            <img src="${user.image}" alt="img"/>
                        </c:if>
                    </div>
                    <div class="ml-4">
                        <div class="font-bold">${account.username}</div>
                    </div>
                </div>
                <ul>
                    <li class="mb-4">
                        <a href="updateuser" class="flex items-center text-gray-700">
                            <i class="fas fa-id-card mr-2"></i>
                            Hồ Sơ
                        </a>
                    </li>
                    <li class="mb-4">
                        <a href="changepassword" class="flex items-center text-gray-700">
                            <i class="fas fa-lock mr-2"></i>
                            Đổi Mật Khẩu
                        </a>
                    </li>
                    <li class="mb-4">
                        <a href="listOrder" class="flex items-center text-gray-700">
                            <i class="fas fa-shopping-cart mr-2"></i>
                            Đơn Mua
                        </a>
                    </li>
                    <li class="mb-4">
                        <a href="userchatwithshop" class="flex items-center text-gray-700">
                            <i class="fas fa-envelope mr-2"></i>
                            Tin nhắn
                        </a>
                    </li>
                    <li class="mb-4">
                        <a href="userVoucher" class="flex items-center text-red-500">
                            <i class="fas fa-gift mr-2"></i>
                            Kho Voucher
                        </a>
                    </li>
                    <li class="mb-4">
                        <a href="home" class="flex items-center text-gray-700">
                            <i class="fas fa-backward mr-2"></i>
                            Trở về trang chủ
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Main Content -->
            <div class="w-3/4 p-6">
                <form action="searchVoucher">
                    <div class="flex items-center mb-4">
                        <input type="text" name="code" placeholder="Nhập mã voucher tại đây" class="border border-gray-300 p-2 w-1/2">
                        <button class="bg-red-500 text-white-50 p-2 ml-2">Tìm kiếm</button>
                    </div>
                </form>
                <div class="flex border-b border-gray-300 mb-4">
                    <a href="#all" class="px-4 py-2" onclick="selected(this, 'all')">Tất Cả</a>
                    <a href="#voucherShopee" class="px-4 py-2" onclick="selected(this, 'voucherShopee')">Hệ thống</a>
                    <a href="#voucherShop" class="px-4 py-2" onclick="selected(this, 'voucherShop')">Shop</a>
                </div>
                <div class="grid grid-cols-2 gap-4" id="all">
                    <!-- Voucher Item -->
                    <c:forEach items="${requestScope.listVoucher}" var="list">
                        <div class="flex border border-gray-300 p-4 bg-white">
                            <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                <div class="text-center mt-6">${list.code}</div>
                            </div>
                            <div class="w-3/4 pl-4">
                                <div class="text-gray-700 font-bold">${list.code}-(<fmt:formatNumber value="${list.reducedAmount}"/>VND)</div>
                                <div class="text-gray-500">${list.description}</div>
                                <span class="bg-red-500 text-white text-xs px-2 py-1">DÀNH CHO BẠN (Số lượng: ${list.quantity})</span>
                                <c:if test="${list.shopName != null}">
                                    <a class="bg-green-500 text-white text-xs px-2 py-1" href="viewshop?sid=${list.getShopId()}">Sử dụng</a>
                                </c:if>
                                <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate}</div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="grid grid-cols-2 gap-4 hidden" id="voucherShop">
                    <!-- Voucher Item -->

                    <c:forEach items="${requestScope.listVoucher}" var="list">
                        <c:if test="${list.shopName != null}">
                            <div class="flex border border-gray-300 p-4 bg-white">
                                <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                    <div class="text-center mt-6">${list.code}</div>
                                </div>
                                <div class="w-3/4 pl-4">
                                    <div class="text-gray-700 font-bold">${list.code}-(<fmt:formatNumber value="${list.reducedAmount}"/>VND)</div>
                                    <div class="text-gray-500">${list.description}</div>
                                    <span class="bg-red-500 text-white text-xs px-2 py-1">DÀNH CHO BẠN (Số lượng: ${list.quantity})</span>
                                    <a class="bg-green-500 text-white text-xs px-2 py-1" href="viewshop?sid=${list.getShopId()}">Sử dụng</a>
                                    <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate}</div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>

                </div>
                <div class="grid grid-cols-2 gap-4 hidden" id="voucherShopee">
                    <!-- Voucher Item -->

                    <c:forEach items="${requestScope.listVoucher}" var="list">
                        <c:if test="${list.shopName == null}">
                            <div class="flex border border-gray-300 p-4 bg-white">
                                <div class="w-1/4 bg-red-500 text-white p-4 relative">
                                    <div class="text-center mt-6">${list.code}</div>
                                </div>
                                <div class="w-3/4 pl-4">
                                    <div class="text-gray-700 font-bold">${list.code}-(<fmt:formatNumber value="${list.reducedAmount}"/>VND)</div>
                                    <div class="text-gray-500">${list.description}</div>
                                    <span class="bg-red-500 text-white text-xs px-2 py-1">DÀNH CHO BẠN (Số lượng: ${list.quantity})</span>
                                    <div class="text-gray-500 text-xs mt-2">Có hiệu lực đến: ${list.endDate}</div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>

                </div>
            </div>
        </div>
    </body>
    <script>
        function selected(element, sectionId) {
            // Remove 'selected' class from all links
            var links = document.querySelectorAll('.flex a');
            links.forEach(function (link) {
                link.classList.remove('selected');
            });

            // Add 'selected' class to the clicked link
            element.classList.add('selected');

            // Hide all sections
            var sections = document.querySelectorAll('#all, #voucherShopee, #voucherShop');
            sections.forEach(function (section) {
                section.classList.add('hidden');
            });

            // Show the selected section
            document.getElementById(sectionId).classList.remove('hidden');
        }
    </script>
</html>
