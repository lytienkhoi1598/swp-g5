<%-- 
    Document   : ConfirmCodeInEmail
    Created on : May 26, 2024, 2:02:52 PM
    Author     : VIET HOANG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirm Code</title>
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    </head>
    <body class="bg-gray-100 flex items-center justify-center min-h-screen">
        <div class="bg-white p-8 rounded-lg shadow-md w-96">
            <div id="FormConfirmCode">
                <div class="flex items-center mb-4">
                    <i class="fas fa-arrow-left text-red-500"></i>
                </div>
                <h2 class="text-center text-xl font-semibold mb-2">Nhập mã xác nhận</h2>
                <p class="text-center text-gray-600 mb-4">Mã xác minh đã được gửi đến Email của bạn</p>
                <form id="verificationForm" action="confirmCode" method="POST">
                    <div class="flex justify-between mb-6">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                        <input type="text" class="w-12 h-12 border-b-2 border-gray-300 text-center text-xl" maxlength="1">
                    </div>
                    <input type="hidden" name="code" value="${sessionScope.code}">
                    <input type="hidden" name="verificationCode" id="verificationCode">
                    <button type="button" class="w-full bg-red-400 text-white py-3 rounded-lg" onclick="submitForm()">KẾ TIẾP</button>
                </form>
            </div>
            <div id="FormInputEmail" style="display: none;">
                <form action="changeEmail">
                    <h2 class="text-center text-xl font-semibold mb-2">Nhập địa chỉ Email</h2>
                    <div class="mb-4">
                        <label for="email" class="block text-sm font-medium text-gray-700">Địa chỉ Email</label>
                        <input type="email" id="email" name="email" class="w-full mt-1 px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" required>
                    </div>
                    <button type="submit" class="w-full bg-red-400 text-white py-3 rounded-lg">Tiếp tục</button>
                </form>
            </div>
                    <p style="color: red; justify-content: center">${requestScope.err}</p>
        </div>
        
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            const inputs = document.querySelectorAll('.w-12');

            inputs.forEach((input, index) => {
                input.addEventListener('input', (e) => {
                    const value = e.target.value;
                    if (value.length === 1) {
                        if (!/\d/.test(value)) {
                            e.target.value = '';
                            return;
                        }
                        if (index < inputs.length - 1) {
                            inputs[index + 1].focus();
                        }
                    }
                });

                input.addEventListener('keydown', (e) => {
                    if (e.key === 'Backspace' && input.value === '' && index > 0) {
                        inputs[index - 1].focus();
                    }
                });
            });
        });

        function submitForm() {
            const inputs = document.querySelectorAll('.w-12');
            let code = '';
            inputs.forEach(input => {
                code += input.value;
            });
            document.getElementById('verificationCode').value = code;
            document.getElementById('verificationForm').submit();
        }

        window.onload = function () {
            var verificationCompleted = ${sessionScope.verificationCompleted };
            var FormConfirmCode = document.getElementById('FormConfirmCode');
            var FormInputEmail = document.getElementById('FormInputEmail');
            if (!verificationCompleted) {
                FormConfirmCode.style.display = 'block';
                FormInputEmail.style.display = 'none';
            } else {
                FormConfirmCode.style.display = 'none';
                FormInputEmail.style.display = 'block';
            }
        };
    </script>
</body>
</html>
