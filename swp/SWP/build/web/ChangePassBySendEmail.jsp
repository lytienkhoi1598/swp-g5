
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
        <link rel="stylesheet" href="<%=request.getContextPath() %>/style/assets/css/stylePageSignIN_UP.css" />

        <title>Change password by email</title>
    </head>
    <body>
        <a href="<%=request.getContextPath() %>/login">← Trở về đăng nhập</a>
        <p>${message}</p>
        <div class="container close" id="container">
            <div class="form-container sign-up-container">
                <h1 class="header--iconclose js--close--pay" style="padding: 16px;position: absolute;top: 0;right: 0;cursor: pointer;"><i class="fa fa-times" style="font-size: 18px;"></i></h1>
            </div>
            <div class="form-container sign-in-container">
            </div>
            <div class="overlay-container">
            </div>
        </div>
        <div class="container" id="containerIn" style="width: 500px; min-height: 480px;">
            <div class="form-container sign-in-container" style="width: 100%;">
                <form action="changepasswordbyemail" method="post" >
                     <h1 style="padding-bottom: 40px;">Đổi mật khẩu bằng email</h1> 
                    <input type="password" class="input-lg form-control"  name="pass" id="password1" placeholder="Mật khẩu mới"  required>
                    <div style="height: 30px;"></div>

                    <input type="password" class="input-lg form-control"  name="cfpass" id="password2" placeholder="Nhập lại mật khẩu"  required>
                    <h5 style="color: red">${requestScope.err}</h5>

                   <button type="submit" style="margin-bottom: 20px">Đổi mật khẩu</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        const signUpButton = document.getElementById('signUp');
        const signInButton = document.getElementById('signIn');
        const container = document.getElementById('container');
        const containerIn = document.getElementById('containerIn');
        const signIn = document.getElementById('in');
        const signUp = document.getElementById('up');

        signUpButton.addEventListener('click', () => {
            container.classList.add('right-panel-active');
        });

        signInButton.addEventListener('click', () => {
            container.classList.remove('right-panel-active');
        });

        signIn.addEventListener('click', () => {
            containerIn.classList.remove('close');
            container.classList.add('close');
        });

        signUp.addEventListener('click', () => {
            container.classList.remove('close');
            containerIn.classList.add('close');
        });
    </script>
</body>


<!--            <div class="container">
                <div style="height: 25px;"></div>
                <div class="row">
                    <div class="text-center">
                        <h1>Đổi mật khẩu bằng email đã gửi</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <p class="text-center"></p>
                        <form action="changepasswordbyemail" method="post" >

                        <input type="password" class="input-lg form-control" name="pass" id="password1" placeholder="Mật khẩu mới"  required>
                        <div style="height: 30px;"></div>

                        <input type="password" class="input-lg form-control" name="cfpass" id="password2" placeholder="Nhập lại mật khẩu"  required>
                        <h5 style="color: red">${requestScope.err}</h5>
                        <div class="row">
                            <div class="col-sm-12">
                                <br>
                                <br>
                                <input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">


                            </div>
                        </div>

                    </form>

                </div>/col-sm-6
            </div>/row
            <div style="height: 100px;"></div>
        </div>-->


</body>
</html>
