<%-- 
    Document   : Home
    Created on : 19 May 2024, 19:11:06
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Change Password</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
        <link rel="stylesheet" href="assets\css\base.css">
        <link rel="stylesheet" href="assets\css\main.css">
        <link rel="stylesheet" href="assets/fonts/fontawesome-free-5.15.3-web/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">


        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    </head>
    <body>
        <header class="header">
            <div class="grid">
                <!-- --------------------------------------------Header Navbar -->
                <jsp:include page="header.jsp"></jsp:include>
                </div>

            </header>
            <!-- End Header -->

            <!-- Begin Container -->
            <div class="container">
                <div style="height: 25px;"></div>
                <div class="row">
                    <div class="text-center">
                        <h1>Đổi mật khẩu</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <p class="text-center"></p>
                        <form action="changepassword" method="post" >
                            <input type="hidden" name="user" value="${sessionScope.user.username}"/>

                        <input type="password" class="input-lg form-control" name="oldpass" id="password1" placeholder="Mật khẩu cũ"  required>
                        <div style="height: 30px;"></div>

                        <input type="password" class="input-lg form-control" name="pass" id="password1" placeholder="Mật khẩu mới"  required>
                        <div style="height: 30px;"></div>

                        <input type="password" class="input-lg form-control" name="cfpass" id="password2" placeholder="Nhập lại mật khẩu"  required>
                        <h5 style="color: red">${requestScope.err}</h5>
                        <div class="row">
                            <div class="col-sm-12">
                                <br>
                                <br>
                                <input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Đang thay đổi mật khẩu..." value="Thay đổi mật khẩu">


                            </div>
                        </div>

                    </form>

                </div><!--/col-sm-6-->
            </div><!--/row-->
            <div style="height: 100px;"></div>
        </div>
        <!-- End Container -->



        <!-- Begin Footer -->
        <jsp:include page="footer.jsp"></jsp:include>
        <!-- End Footer -->

    </body>
</html>
