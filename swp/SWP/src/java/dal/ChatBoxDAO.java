/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.ChatBox;
import model.Comment;
import model.Shop;
import model.User;

/**
 *
 * @author admin
 */
public class ChatBoxDAO extends DBContext {

    public List<ChatBox> getAllChatOfUserAndShop(int uaccountId, int saccountId) {
        List<ChatBox> list = new ArrayList<>();
        String sql = "select chatBox.chatId, shop.shopId, shop.shopName,shop.image as shopImage, [user].userId, [user].fullName, [user].image as userImage from chatBox\n"
                + "join [user] on [user].userId = chatBox.userId\n"
                + "join shop on shop.shopId = chatBox.shopId\n"
                + "where [user].accountId = ? or shop.accountId =?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uaccountId);
            st.setInt(2, saccountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int chatId = rs.getInt("chatId");
                int shopId = rs.getInt("shopId");
                int userId = rs.getInt("userId");
                String shopName = rs.getString("shopName");
                String images = rs.getString("shopImage");
                String fullName = rs.getString("fullName");
                String imageu = rs.getString("userImage");

                Shop shop = new Shop(shopId, shopName, images);
                User user = new User(userId, fullName, imageu);

                ChatBox chatBox = new ChatBox(chatId, userId, shopId, shop, user);
                list.add(chatBox);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public Boolean getChatBoxByAccountIdAndShopId(int accountId, int shopId) {
        String sql = "select * from chatBox\n"
                + "join [user] on [user].userId =chatBox.userId\n"
                + "where [user].accountId=? and chatBox.shopId =?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, accountId);
            st.setInt(2, shopId);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    int chatId = rs.getInt("chatId");
                    int userId = rs.getInt("userId");        
                    String fullName = rs.getString("fullName");
                    String userImage = rs.getString("image");

                    User user = new User(userId, fullName, userImage,accountId);
                    ChatBox chatbox = new ChatBox(chatId, userId, shopId, user);

                    return true;
                }
            }
        } catch (SQLException e) {
            System.err.println("Lỗi cơ sở dữ liệu: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
    public Boolean getChatBoxByUserIdAndAccountId(int userId, int accountId) {
        String sql = "select * from chatBox\n" +
"                join [user] on [user].userId =chatBox.userId\n" +
"           join shop on shop.shopId=chatBox.shopId\n" +
"		   where chatBox.userId=? and shop.accountId =?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, userId);
            st.setInt(2, accountId);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    int chatId = rs.getInt("chatId");
                    int shopId = rs.getInt("shopId");        
                    String fullName = rs.getString("fullName");
                    String userImage = rs.getString("image");

                    User user = new User(userId, fullName, userImage,accountId);
                    ChatBox chatbox = new ChatBox(chatId, userId, shopId, user);

                    return true;
                }
            }
        } catch (SQLException e) {
            System.err.println("Lỗi cơ sở dữ liệu: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
    public void deleteChatBoxBychatId(int chatId) {
        String sql = "delete from chatBox where chatId=?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, chatId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        ChatBoxDAO c = new ChatBoxDAO();
        System.out.println(c.getAllChatOfUserAndShop(0, 3));

    }
}
