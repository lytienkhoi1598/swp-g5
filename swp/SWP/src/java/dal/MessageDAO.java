package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import model.ChatBox;
import model.Message;
import model.Shop;
import model.User;

public class MessageDAO extends DBContext {

    public List<Message> getAllMessageByChatId(int chatId) {
        List<Message> list = new ArrayList<>();
        String sql = "select message.*, shop.shopId, shop.image as shopImage, shop.shopName, "
                + "[user].userId, [user].fullName, [user].image as userImage "
                + "from message "
                + "join chatBox on chatBox.chatId = message.chatId "
                + "join [user] on [user].userId = chatBox.userId "
                + "join shop on shop.shopId = chatBox.shopId "
                + "where message.chatId= ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, chatId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int shopId = rs.getInt("shopId");
                int userId = rs.getInt("userId");
                String shopName = rs.getString("shopName");
                String shopImage = rs.getString("shopImage");
                String fullName = rs.getString("fullName");
                String userImage = rs.getString("userImage");
                int messageId = rs.getInt("messageId");
                int senderId = rs.getInt("senderId");
                String content = rs.getString("content");
                LocalTime sentTime = rs.getTime("sentTime").toLocalTime(); // Changed to LocalTime

                Shop shop = new Shop(shopId, shopName, shopImage);
                User user = new User(userId, fullName, userImage);
                Message message = new Message(messageId, chatId, sentTime, senderId, content, shop, user);
                list.add(message);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<Message> getListMessageByShopIdAndAccountId(int shopId, int accountId) {
        List<Message> list = new ArrayList<>();
        String sql = "select message.*, shop.shopId, shop.image as shopImage, shop.shopName, [user].fullName, [user].userId, [user].image as userImage from message\n"
                + "join chatBox on chatBox.chatId = message.chatId\n"
                + "join [user] on [user].userId = chatBox.userId\n"
                + "join shop on shop.shopId = chatBox.shopId\n"
                + "where chatBox.shopId=? and [user].accountId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, shopId);
            st.setInt(2, accountId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt("userId");
                int chatId = rs.getInt("chatId");
                String shopName = rs.getString("shopName");
                String shopImage = rs.getString("shopImage");
                String fullName = rs.getString("fullName");
                String userImage = rs.getString("userImage");
                int messageId = rs.getInt("messageId");
                int senderId = rs.getInt("senderId");
                String content = rs.getString("content");
                LocalTime sentTime = rs.getTime("sentTime").toLocalTime(); // Changed to LocalTime

                Shop shop = new Shop(shopId, shopName, shopImage);
                User user = new User(userId, fullName, userImage);
                Message message = new Message(messageId, chatId, sentTime, senderId, content, shop, user);
                list.add(message);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }
    public List<Message> getListMessageByUserIdAndAccountId(int userId, int accountId) {
        List<Message> list = new ArrayList<>();
        String sql = "select message.*, shop.shopId, shop.image as shopImage, shop.shopName, [user].fullName, [user].userId, [user].image as userImage from message\n"
                + "join chatBox on chatBox.chatId = message.chatId\n"
                + "join [user] on [user].userId = chatBox.userId\n"
                + "join shop on shop.shopId = chatBox.shopId\n"
                + "where chatBox.userId=? and shop.accountId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, accountId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int shopId = rs.getInt("shopId");
                int chatId = rs.getInt("chatId");
                String shopName = rs.getString("shopName");
                String shopImage = rs.getString("shopImage");
                String fullName = rs.getString("fullName");
                String userImage = rs.getString("userImage");
                int messageId = rs.getInt("messageId");
                int senderId = rs.getInt("senderId");
                String content = rs.getString("content");
                LocalTime sentTime = rs.getTime("sentTime").toLocalTime(); // Changed to LocalTime

                Shop shop = new Shop(shopId, shopName, shopImage);
                User user = new User(userId, fullName, userImage);
                Message message = new Message(messageId, chatId, sentTime, senderId, content, shop, user);
                list.add(message);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public void addChatBoxOfUser(int accountId, int shopId) {
        try {
            String sql = "WITH UserCTE AS (\n"
                    + "    SELECT userId\n"
                    + "    FROM [user]\n"
                    + "    WHERE accountId = ?\n"
                    + ")\n"
                    + "INSERT INTO chatBox(userId, shopId)\n"
                    + "SELECT userId, ?\n"
                    + "FROM UserCTE;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, shopId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void addMessageOfUser(int shopId, int accountId, int senderId, String content) {
        try {
            String sql2 = "WITH ChatCTE AS (\n"
                    + "                     SELECT chatId\n"
                    + "                       FROM chatBox\n"
                    + "					   join [user] on chatBox.userId = [user].userId\n"
                    + "                        WHERE shopId = ? and [user].accountId= ?\n"
                    + "                    )\n"
                    + "                    INSERT INTO message(chatId, senderId, content)\n"
                    + "                    SELECT chatId, ?, ?\n"
                    + "                    FROM ChatCTE;";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, shopId);
            st2.setInt(2, accountId);
            st2.setInt(3, senderId);
            st2.setString(4, content);
            st2.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void addChatBoxOfShop(int accountId, int userId) {
        try {
            String sql = "WITH ShopCTE AS (\n"
                    + "    SELECT shopId\n"
                    + "    FROM shop\n"
                    + "    WHERE accountId = ?\n"
                    + ")\n"
                    + "INSERT INTO chatBox(userId, shopId)\n"
                    + "SELECT ?, shopId\n"
                    + "FROM ShopCTE;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, userId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    public void addMessageOfShop(int userId, int accountId, int senderId, String content) {
        try {
            String sql2 = "WITH ChatCTE AS (\n"
                    + "                     SELECT chatId\n"
                    + "                       FROM chatBox\n"
                    + "					   join [user] on chatBox.userId = [user].userId\n"
                    + "					   join shop on shop.shopId=chatBox.shopId\n"
                    + "                        WHERE chatBox.userId = ? and shop.accountId= ?\n"
                    + "                    )\n"
                    + "                    INSERT INTO message(chatId, senderId, content)\n"
                    + "                    SELECT chatId, ?, ?\n"
                    + "                FROM ChatCTE;";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, userId);
            st2.setInt(2, accountId);
            st2.setInt(3, senderId);
            st2.setString(4, content);
            st2.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteMessageBychatId(int chatId) {
        String sql = "delete from message where chatId=?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, chatId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteMessageByMessageId(int messageId) {
        String sql = "delete from message where messageId=?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, messageId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void updateMessageByMessageId(String content, int messageId) {
        String sql = "update message set content = ? where messageId = ?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, content);
            st.setInt(2, messageId);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MessageDAO m = new MessageDAO();
        System.out.println(m.getListMessageByShopIdAndAccountId(5, 4));
    }
}
