/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Color;
import model.Comment;
import model.User;

/**
 *
 * @author admin
 */
public class CommentDAO extends DBContext {

    public List<Comment> getAllCommentByShopProductId(int id) {
        List<Comment> list = new ArrayList<>();
        String sql = "select * from comment\n"
                + "join orderDetail on comment.orderDetailId=orderDetail.orderDetaiId\n"
                + "join productItem on productItem.productItemId= orderDetail.productItemId\n"
                + " JOIN account ON account.accountId = comment.accountId\n"
                + "JOIN [user] ON [user].accountId = account.accountId\n"
                + "JOIN rating on rating.ratingId = comment.ratingId\n"
                + "JOIN size on size.sizeId = productItem.sizeId\n"
                + "JOIN color on color.colorId = productItem.colorId\n"
                + "where comment.shopProductId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int commentId = rs.getInt("commentId");
                int accountId = rs.getInt("accountId");
                int shopProductId = rs.getInt("shopProductId");
                String content = rs.getString("content");
                String username = rs.getString("username");
                Date created_at = rs.getDate("created_at");
                Date updated_at = rs.getDate("updated_at");
                String fullName = rs.getString("fullName");
                String image = rs.getString("image");
                int starRating = rs.getInt("starRating");
                String colorValue = rs.getString("colorValue");
                String sizeValue = rs.getString("sizeValue");

                User user = new User(id, fullName, image, accountId);
                Account account = new Account(accountId, username);
                Comment comment = new Comment(commentId, accountId, shopProductId, created_at, updated_at, content, account, user, starRating, colorValue, sizeValue);
                list.add(comment);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public void addCommentOfUser(int userId, int starRating, int accountId, int shopProductId, String content, int orderDetailId) {

        try {
            String sql = "INSERT INTO comment (accountId, shopProductId, content)\n"
                    + "                  VALUES (?, ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, shopProductId);
            st.setString(3, content);
            st.executeUpdate();

            String sql2 = "WITH MaxComment AS (\n"
                    + "                    SELECT MAX(commentId) AS largestCommentId\n"
                    + "                     FROM comment\n"
                    + "                    )\n"
                    + "                  \n"
                    + "                   INSERT INTO rating (shopProductId, userId, starRating,orderDetailId, commentId)\n"
                    + "                                SELECT ?, ?, ?, ?,largestCommentId\n"
                    + "                    FROM MaxComment;";

            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, shopProductId);
            st2.setInt(2, userId);
            st2.setInt(3, starRating);
            st2.setInt(4, orderDetailId);

            st2.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public Comment countCommentByShopProduct(int shopProductid) {
        String sql = "SELECT COUNT(content) AS num_contents\n"
                + "FROM comment\n"
                + "where shopProductId =?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, shopProductid);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return new Comment(rs.getInt("num_contents"));
                }
            }
        } catch (SQLException e) {
            System.err.println("Lỗi cơ sở dữ liệu: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
 public void updateContentByCommentId(int commentId, String content) {
        String sql = "update comment set content = ? where commentId = ?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, content);
            st.setInt(2, commentId);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CommentDAO c = new CommentDAO();
        System.out.println(c.countCommentByShopProduct(1));
        System.out.println("hihi");
        System.out.println("kakaa");
    }
}
